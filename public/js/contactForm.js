var contactFunct = (function()
{
    var validateFields = function() {

        var requiredInputs = $(".required");
        var emptyRequiredFields = requiredInputs.filter(function(){ return $(this).val() == '' || $(this).val() == null });
        var filledRequiredFields = requiredInputs.filter(function() { return $(this).val() != '' || $(this).val() == null });
        var okToSave = true;

        filledRequiredFields.each(function() {
            $(this).siblings('.required-text').hide();
        });

        if(emptyRequiredFields.length > 0) {
            emptyRequiredFields.each(function(elem) {
                $(this).siblings('.required-text').show();
            });
            okToSave = false;
        }
        return okToSave;

    };

    return { okToSave   : validateFields };

})();
    /*var errors = false;
    $('.required').parent().find('.input').on('blur', function() {
        var error_div = $(this).parent().find('.error_message');
        var field_container = $(this).parent();
        if (!$.empty_field_validation($(this).val())) {
            error_div.html('This field is required.');
            //error_div.css('display', 'block');
            //field_container.addClass('error');
            errors = true;
        } else {
            error_div.html('');
            //error_div.css('display', 'none');
            //field_container.removeClass('error');
            errors = false;
        }
    });

    $('#email').on('blur', function(){
        var error_div = $(this).parent().find('.error_message');
        var field_container = $(this).parent();
        if (!$.email_validation($(this).val())) {
            error_div.html('Expected Input: email');
            error_div.css('display', 'block');
            field_container.addClass('error');
            errors = true;
        } else {
            error_div.html('');
            error_div.css('display', 'none');
            field_container.removeClass('error');
            errors = false;
        }
    });*/



/*
$.empty_field_validation = function(field_value) {
    if (field_value.trim() == '') return false;
    return true;
}

$.email_validation = function(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
} */