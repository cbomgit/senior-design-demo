/**
 * Created by boman on 3/23/2017.
 */
var fields = [];
var fieldsLookup = {};
var originalStartDate = null;

$(document).ready(function() {
    fields = [
        new Field({
            name:"EventTitle",
            required: true,
            selector:"#EventTitle",
            getValue:function() {
                return $(this.selector).val();
            },
            setValue: function(newTitle) {
                $(this.selector).val(newTitle);
            },
            hasGoodInput: function(){
                return this.getValue() != '';
            },
            errorMessage: 'Event title is required'
        }),
        new Field({
            name:"EventStart",
            required:true,
            selector:"#EventStart",
            init: function() {
                $(this.selector).datepicker({
                    dateFormat: "yy-mm-dd",
                    currentText: "Now",
                    showButtonPanel: true,
                    appendText: "(yyyy-mm-dd)",
                    constrainInput: true,
                    changeMonth: true,
                    changeYear: true
                });

                var startDate = $(this.selector);
                if(startDate.datepicker("getDate") == null) {
                    var date = new Date();
                    var queryString = window.location.href.split("?");
                    if(queryString.length > 1 ) {
                        //split the string in case there was more than 1 url param
                        var queryValues = queryString[1].split("&");

                        for(var i = 0; i < queryValues.length; i++) {
                            var query = queryValues[i].split("=");
                            if(query.length == 2) {
                                if(query[0].toLowerCase() == 'startdate') {
                                    date = query[1].split("T")[0];
                                    if(query[1].split("T")[1] != null) {
                                        $("#EventFrom").timepicker("setTime", query[1].split("T")[1]);
                                    }
                                }
                            } else {
                                //incomplete query parm, return to document.referrer
                                window.location = document.referrer;
                            }
                        }

                    }

                    startDate.datepicker("setDate", date);
                }
            },
            getValue:function() {
                return moment($(this.selector).datepicker("getDate"))
            },
            setValue:function(momentObj) {
                $(this.selector).datepicker("setDate", momentObj.format("YYYY-MM-DD"));
            },
            hasGoodInput:function() {
                return $(this.selector).val() != '' && $(this.selector).val() != null;
            },
            errorMessage:'This field is required'
        }),
        new Field({
            name:"calendar-select",
            required:true,
            selector:"#calendar-select",
            init: function() {
                if($(this.selector).find("option").length == 0) {
                    $("#informational-text").show();
                }
            },
            getValue:function() {
                return $(this.selector).val();
            },
            setValue: function(calendar) {
                $(this.selector).val(calendar);
            },
            hasGoodInput: function(){
                return this.getValue() != '';
            },
            errorMessage:'You must select a calendar'
        }),
        new Field({
            name:"category-select",
            required:true,
            selector:"#category-select",
            init: function() {
                if($(this.selector).find("option").length == 0) {
                    $("#informational-text").show();
                }
            },
            getValue:function() {
                return $(this.selector).val();
            },
            setValue: function(category) {
                $(this.selector).val(category);
            },
            hasGoodInput: function(){
                return this.getValue() != '';
            },
            errorMessage:'You must select an event category'
        }),
        new Field({
            name:"AllDayEvent",
            required:false,
            selector:"#AllDayEvent",
            onChange: function() {

                var isAllDayEvent = fieldsLookup['AllDayEvent'].getValue();
                if(isAllDayEvent)
                {
                    $(".event-time-fields").hide(200);
                    $("#EventFrom").val("00:00:00").prop("disabled", true);
                    $("#EventTo").val("24").prop("disabled", true).removeClass("required");
                    $("#num-days-container").show(200);
                    $("#NumDays").addClass("validate-int");

                }
                else
                {
                    $(".event-time-fields").show(200);
                    $("#EventFrom").timepicker("setTime", "").prop("disabled", false);
                    $("#EventTo").val("0").prop("disabled", false).addClass("required");
                    $("#num-days-container").hide(200);
                    $("#NumDays").removeClass("validate-int");
                }
            },
            init:this.onChange,
            getValue: function() {
                return $(this.selector).prop("checked");
            },
            setValue: function(value) {
                $(this.selector).prop("checked", value);
            }
        }),
        new Field({
            name:"EventFrom",
            required:function() {
                return !fieldsLookup['AllDayEvent'].getValue()
            },
            selector: "#EventFrom",
            init:function() {
                $(this.selector).timepicker({
                    "disableTouchKeyboard":true,
                    "typeaheadHighlight":true
                });
            },
            getValue:function() {
                return moment($(this.selector).timepicker("getTime")).format("HH:mm:ss");
            },
            setValue:function(time) {
                $(this.selector).timepicker("setTime", time);
            },
            hasGoodInput: function() {
                return moment($(this.selector).timepicker("getTime")).isValid();
            },
            errorMessage:'The start hour is required or must be a time value'
        }),
        new Field({
            name: "EventTo",
            selector: "#EventTo",
            required: function() {
                return !fieldsLookup['AllDayEvent'].getValue()
            },
            init: function() {
                //empty
            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(duration) {
                $(this.selector).val(duration);
            },
            hasGoodInput: function() {
                var isFloat = /^\d+(\.\d+)*$/;
                var currentVal = this.getValue();
                return isFloat.test(currentVal) && parseFloat(currentVal) > 0;
            },
            errorMessage:'Event length is required/is invalid'
        }),
        new Field({
            name:"NumDays",
            selector:"#NumDays",
            required: function() {
                return fieldsLookup['AllDayEvent'].getValue();
            },
            init: function() {

            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(numDays) {
                $(this.selector).val(numDays);
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9][0-9]*$/;
                var currentVal = this.getValue();
                return isInteger.test(currentVal) && parseInt(currentVal) > 0;
            },
            onChange: function() {
                if(fieldsLookup['AllDayEvent'].getValue()) {
                    var totalHours = parseInt(this.getValue()) * 24;
                    fieldsLookup['EventTo'].setValue(totalHours);
                }
            },
            errorMessage:'Number of days is required'
        }),
        new Field({
            name:"recurrence-type",
            selector: "#recurrence-type",
            required: true,
            onChange: function() {
                var recurrenceType = $(this).val();
                var globals = $(".global-recurrence-options");
                $(".recurrence-options:not(#"+recurrenceType+")").hide(200).removeClass("selected-recurrence-type");
                $("#"+recurrenceType).show(200).addClass("selected-recurrence-type");
                recurrenceType == 'recur-none' ? globals.hide(200) : globals.show(200);

                if(recurrenceType == 'only-weekdays') {
                    moment.weekdaysShort().forEach(function(item) {
                        $("[name='weekday']").filter(function() {
                            return $(this).val() == item
                        }).prop("checked", true);
                    });
                }
                else {
                    moment.weekdaysShort().forEach(function(item) {
                        $("[name='weekday']").filter(function() {
                            return $(this).val() == item
                        }).prop("checked", false);
                    });
                }
            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(value) {
                $(this.selector).val(value);
            },
            hasGoodInput: function(){return true;},
            errorMessage: 'Required'
        }),
        new Field({
            name:"daily-recurrence-value",
            selector:"#daily-recurrence-value",
            required: function() {
                return fieldsLookup['recurrence-type'].getValue() == 'daily-recurrence'
            },
            init: function() {
                this.setValue('1');
            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(frequency) {
                $(this.selector).val(frequency);
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9][0-9]*$/;
                var currentVal = this.getValue();
                return isInteger.test(currentVal) && parseInt(currentVal) > 0;
            },
            errorMessage: 'Must be a whole number'
        }),
        new Field({

            name:"weekly-recurrence",
            selector:"#weekly-recurrence",
            required: function() {
                return fieldsLookup['recurrence-type'].getValue() == 'weekly-recurrence'
            },
            init: function() {
                this.setValue('1');
            },
            getValue: function() {
                var days = [];
                $(this.selector).find("input[name='weekday']:checked").each( function() {
                    days.push($(this).val())
                });
                return days;
            },
            setValue: function(frequency) {
                //$(this.selector).val(frequency);
            },
            hasGoodInput: function() {
                return this.getValue().length > 0;
            },
            errorMessage:'Make at least one selection'
        }),
        new Field({
            name:"monthly-recurrence-value",
            selector:"#monthly-recurrence-value",
            required: function() {
                return fieldsLookup['recurrence-type'].getValue() == 'monthly-recurrence'
            },
            init: function() {
                this.setValue('1');
            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(frequency) {
                $(this.selector).val(frequency);
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9][0-9]*$/;
                var currentVal = this.getValue();
                return isInteger.test(currentVal) && parseInt(currentVal) > 0;
            },
            errorMessage: 'Must be a whole number'
        }),
        new Field({

            name:"yearly-recurrence-value",
            selector:"#yearly-recurrence-value",
            required: function() {
                return fieldsLookup['recurrence-type'].getValue() == 'yearly-recurrence'
            },
            init: function() {
                this.setValue('1');
            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(frequency) {
                $(this.selector).val(frequency);
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9][0-9]*$/;
                var currentVal = this.getValue();
                return isInteger.test(currentVal) && parseInt(currentVal) > 0;
            },
            errorMessage: 'Non-sensical or blank value'
        }),
        new Field({
            name: "global-recurrence-options",
            selector:".global-recurrence-options",
            required: false,
            onChange:function() {
                var lengthType = $("input[name='recurrence-length-option']:checked").val();
                var optionToggle = lengthType == 'fixed';
                $("#recur-until-date").prop("disabled", optionToggle);
                $("#recur-n-times").prop("disabled", !optionToggle);

            },
            hasGoodInput: function() {
                return $(this.selector).find("input[type='radio']:checked").length != 0;
            },
            getValue: function() {
                return $(this.selector + " input[type='radio']:checked").val();
            },
            setValue: function(value) {

                if(value == "fixed" || value == "date") {
                    $(this.selector + " input[type='radio']").filter(function(){
                        return $(this).val() == value;
                    }).prop("checked", true)
                        .filter(function() {
                            return $(this).val() != value;
                        }).prop("checked", false);
                }

            }
        }),
        new Field({
            name: "recur-until-date",
            selector: "#recur-until-date",
            required: function() {
                return fieldsLookup['recurrence-type'].getValue() != 'recur-none' &&
                    fieldsLookup['global-recurrence-options'].getValue() == "date";
            },
            init: function() {
                $(this.selector).datepicker({
                    dateFormat: "yy-mm-dd",
                    currentText: "Now",
                    showButtonPanel: true,
                    appendText: "(yyyy-mm-dd)",
                    constrainInput: true,
                    changeMonth: true,
                    changeYear: true
                }).datepicker("setDate", moment().format("YYYY-MM-DD"));
            },
            onChange: function() {
                fieldsLookup['global-recurrence-options'].setValue("date");
            },
            hasGoodInput: function() {
                var startDate = fieldsLookup['EventStart'].getValue();
                return this.getValue().isValid() && this.getValue().isAfter(startDate);
            },
            getValue: function() {
                return moment($(this.selector).datepicker("getDate"));
            },
            errorMessage: 'You cannot repeat into the past or into nothing.'
        }),
        new Field({
            name: "recur-n-times",
            selector: "#recur-n-times",
            required: function() {
                return fieldsLookup['recurrence-type'].getValue() != 'recur-none' &&
                    fieldsLookup['global-recurrence-options'].getValue() == "fixed";
            },
            init: function() {
                this.setValue(0);
            },
            onChange: function() {
                fieldsLookup['global-recurrence-options'].setValue("fixed");
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9]+$/;
                return isInteger.test(this.getValue());
            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(days) {
                var isInteger = /^[1-9][0-9]*$/;
                if(isInteger.test(days)) {
                    $(this.selector).val(days);
                }
            },
            errorMessage: 'Why would you create a recurring event that repeats zero times?'
        }),
        new Field({
            name:"contact-filter",
            selector:"#contact-filter",
            required: false,
            init: function() {
                $(this.selector).on('keyup past', function() {
                    //get the filter value
                    var filterValue = $("#contact-filter").val().toLowerCase();
                    var contactPicker = $("#contact-select");
                    //hide all list items that don't match the filter value
                    contactPicker.find("li").filter(function() {
                        return $(this).html().toLowerCase().indexOf(filterValue) == -1;
                    }).hide();

                    //show all elements that do possibly match the filter value
                    contactPicker.find("li").filter(function() {
                        return $(this).html().toLowerCase().indexOf(filterValue) != -1;
                    }).show();
                })
            }
        })
    ];

    fieldsLookup = {};
    for(var i = 0; i < fields.length; i++) {
        fieldsLookup[fields[i].name] = fields[i];
    }

    originalStartDate = moment(fieldsLookup['EventStart'].getValue().format("YYYY-MM-DD") + " " + fieldsLookup['EventFrom'].getValue());
});

