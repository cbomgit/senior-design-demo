/**
 * Created by christian on 2/10/17.
 */
var fields = [];
var fieldsLookup = {};

function validate() {

    //get the required fields
    var requiredFields = fields.filter(function(item) {
        return item.isRequired();
    });


    canSave = true;
    requiredFields.forEach(function(item) {
        if(item.hasGoodInput()) {
            canSave = canSave && true; //don't set canSave to true if it's already false
            item.hideErrorMessage();
        }
        else {
            canSave = false;
            item.showErrorMessage();
        }
    });

    return canSave;
}

$(document).ready(function() {
    fields = [
        new Field({
            name:"name",
            selector:"#name",
            required: true,
            init: function() {

            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(value) {
                $(this.selector).val(value);
            },
            hasGoodInput: function() {
                return this.getValue() != '';
            },
            errorMessage: "Name is required"
        }),
        new Field({
            name:'color',
            selector: '#color',
            required: true,
            init: function() {
                $("#color").spectrum({
                    flat: false,
                    showInput: true,
                    allowEmpty: false,
                    showAlpha: false,
                    replacerClassName: "form-control",
                    preferredFormat: "hex"
                });
            },
            getValue: function() {
                return $(this.selector).spectrum("get").toHex();
            },
            setValue: function(value) {
                $(this.selector).spectrum("set", value);
            },
            hasGoodInput: function() {
                return this.getValue() != '';
            },
            errorMessage: "Color is required"
        }),
        new Field({
            name:'default_length',
            selector: '#default_length',
            required: true,
            init: function() {

            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(value) {
                $(this.selector).val(value);
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9][0-9]*$/;
                return isInteger.test(this.getValue());
            },
            errorMessage: "Default length is required/cannot be zero"
        }),
        new Field({
            name:'default_capacity',
            selector: '#default_capacity',
            required: true,
            init: function() {

            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(value) {
                $(this.selector).val(value);
            },
            hasGoodInput: function() {
                var isInteger = /^[1-9][0-9]*$/;
                return isInteger.test(this.getValue());
            },
            errorMessage: "Default capacity is required/cannot be zero"
        }),
        new Field({
            name:'default_token_cost',
            selector: '#default_token_cost',
            required: true,
            init: function() {

            },
            getValue: function() {
                return $(this.selector).val();
            },
            setValue: function(value) {
                $(this.selector).val(value);
            },
            hasGoodInput: function() {
                var isFloat = /^\d+(\.\d+)*$/;
                var currentVal = this.getValue();
                return isFloat.test(currentVal) && parseFloat(currentVal) > 0;
            },
            errorMessage: "Token cost is required"
        })
    ];

    fieldsLookup = {};
    for(var i = 0; i < fields.length; i++) {
        fieldsLookup[fields[i].name] = fields[i];
    }
});