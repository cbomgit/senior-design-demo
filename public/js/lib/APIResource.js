/**
 * Created by boman on 3/23/2017.
 */
function API (resourceName, apiToken) {

    var baseUrl = '/api/1.0';

    this.getAll = function(queryString) {
        var options = {
            url: baseUrl + '/' + resourceName + '?api_token='+apiToken+ '&'+queryString,
        };
        return $.get(options);
    };

    this.get = function(id) {
        var options = {
            url: baseUrl + '/' + resourceName + '/' + id + '?api_token='+apiToken,
        };
        return $.get(options);
    };

    this.post = function(requestBody) {
        var options = {
            url: baseUrl + '/' + resourceName + '?api_token='+apiToken,
            method: 'POST',
            data: requestBody
        };
        return $.ajax(options);
    };

    this.put = function(id, requestBody) {
        var options = {
            url: baseUrl + '/' + resourceName + '/' + id + '?api_token='+apiToken,
            method: 'PUT',
            data: requestBody
        };
        return $.ajax(options);
    };

    this.delete = function(id) {
        var options = {
            url: baseUrl + '/' + resourceName + '/' + id + '?api_token='+apiToken,
            method: 'DELETE'
        };
        return $.ajax(options);
    };

}

function Field (config) {


    /************ private variables/methods **************/

    //true, false, or function that returns true or false
    var required = config.required;

    //error message for this field
    var errorMessage = config.errorMessage;


    /************ public variables/methods ***************/

    //name of this field - not used for anything
    this.name = config.name;

    //selector identifying this field
    this.selector = config.selector;

    //function that returns this fields value
    //dates return moment objects
    //radio and checkbox elements return arrays of strings
    //all other data types return strings
    this.getValue = config.getValue;

    //function that sets this fields value
    this.setValue = config.setValue;

    //function object describes how the field should behave if it is altered,
    this.onChange = config.onChange;

    //function that returns true if input has valid contents, otherwise return false
    this.hasGoodInput = config.hasGoodInput;

    //evaluates and returns required variable
    this.isRequired = function() {
        if(typeof(required) == "function"){
            return required();
        }
        else {
            return required;
        }
    };


    this.showErrorMessage = function() {
        $(this.selector + '-error').show();
    };

    this.hideErrorMessage = function() {
        $(this.selector + '-error').hide();
    };

    //constructor actions
    if(typeof config.init == 'function') {
        config.init();
    }

    $(this.selector).change(this.onChange);
    if(errorMessage) {
        $(this.selector).after('<span id="'+this.name+'-error" class="required-text">'+errorMessage+'</span>');
    }
}