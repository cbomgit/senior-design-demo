/**
 * Created by christian on 2/10/17.
 */


var formSaver = (function() {

    var token = $("span#api_token").attr("_token");
    var resourceType = '';
    var resourceId = '';
    var method = '';

    var postSucceeded = function(response) {

        $("#ajax-message").html("Done!");
        if(formSaver.returnurl == undefined || formSaver.returnurl == '') {
            navigateToReturnURL();
        }
        else {
            window.location = formSaver.returnurl;
        }

    };

    var navigateToReturnURL = function() {
        var queryString = window.location.href.split("?");
        if(queryString.length < 2 ) {
            //not returnUrl parameter passed, navigate to the dashboard by default
            window.location = "/dashboard";
        }
        else {
            //split the string in case there was more than 1 url param
            var queryValues = queryString[1].split("&");

            var returnUrl = '';

            for(var i = 0; i < queryValues.length; i++) {
                var query = queryValues[i].split("=");
                if(query.length == 2) {
                    if(query[0].toLowerCase() == 'returnurl') {
                        returnUrl = query[1];
                    }
                } else {
                    //incomplete query parm, return to document.referrer
                    window.location = document.referrer;
                }
            }

            window.open(returnUrl + "?q=" + new Date().getTime(), '_self');
        }
    };

    var postFailed = function(error) {

        document.getElementById('error-text').innerHTML = error.responseText;
    };

    var save = function(ajaxBody) {

        $("#ajax-message").html("Saving...").show();
        var url = '/api/1.0/'+formSaver.type+ '' + formSaver.id + '?api_token=' + token;

        $.ajax({
            "url":url,
            "method": formSaver.method,
            "headers": {
                "Accept": "application/json",
                "content-type": "application/x-www-form-urlencoded"
            },
            "data": ajaxBody,
            success: formSaver.successCallback,
            error: postFailed

        });
    };

    var deleteForm = function() {

        $("#ajax-message").html("Deleting...").show();
        var baseUrl = '/api/1.0/'+ formSaver.type + '' + formSaver.id + '?api_token=' + token;
        $.ajax({
            "url": baseUrl,
            "method":"DELETE",
            "headers":{
                "Accept":"application/json"
            },
            success: postSucceeded,
            error: postFailed
        });
    };

    //return 'public' object properties and functions. These will be accessible outside of the script
    //by using formSaver.<method | property>
    return {
        save            : save,                //function to save the form
        delete          : deleteForm,          //function to delete the form
        successCallback : postSucceeded,       //function to call when ajax method succeeds
        method          : method,              //HTTP method to use in save()
        id              : resourceId,          //id of the resource to modify
        type            : resourceType,        //type of resource to create or modify
        navToReturnUrl  : navigateToReturnURL, //function navigates to returnurl param passed in url
        returnurl       : ''
    };

})();
