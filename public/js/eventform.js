/**
 * Created by christian on 1/30/17.
 */


/*this stores functions that handle general form logic. It returns public functions that are required
    on the form itself.

    To initialize, call formConfig.init();
    */
var formConfig = (function()
{



    var deleteAttendance = function() {

        //delete an attendance record for an event
        var parent = $(this).parents(".contact-list-item");
        var attendanceId  = parent.data("attendance-id");
        var token = $("span#api_token").attr("_token");
        var attAPI = new API('attendance', token);

        attAPI.delete(attendanceId)
            .then(function(response) {
                var id = response['payload']['id'];
                $(".contact-list-item")
                    .filter(function() { return $(this).data("attendance-id") == id }) //get matching attendance elem
                    .removeClass('current-attendee') //unselect it as a current attendee
                    .removeClass('ui-selected')      //remove selection styles
                    .addClass('available-attendee')  //make it an available attendee
                    .find(".attendance-form").hide();//hide the attendance form since it's showing
            });

    };

    //highlight the contacts that have already been added to an edvent.
    var highlightCurrentAttendees = function() {

        for(var i = 0; i < attendances.length; i++) {

            var matchingContact = $("#contact-select")
                .find(".contact-list-item")
                .filter(function() {
                    return $(this).data("contact-id") == attendances[i]['contact'];
                });

            $(matchingContact).toggleClass('ui-selected current-attendee')
                .toggleClass("available-attendee")
                .data("attendance-id", attendances[i]['id']);

            $(matchingContact).find('.attendee-refund-percentage').val(attendances[i]['refund_percent']);
            $(matchingContact).find('.attendee-status').val(attendances[i]['status']);
            $(matchingContact).find('.attendance-form-notes').val(attendances[i]['notes']);
            $(matchingContact).find('.delete-attendance-link').click(deleteAttendance);

        }
    };

    //clear the filter and show all contacts
    var showAllContacts = function() {

        document.getElementById('contact-filter').value = '';
        $(".contact-list-item").show(200);

    };

    //clear the current filter value and hide non-selected attendees
    //also show current attendees if any were hidden by previous filter value
    var viewAttending = function() {

        document.getElementById('contact-filter').value = '';
        $(".available-attendee").hide(200);
        $(".current-attendee").show(200);

    };

    var showAttendanceDialog = function() {

        var parentElem = $(this).parents(".contact-list-item");

        if(parentElem.attr("class").indexOf("current-attendee") > -1 ||
           parentElem.attr("class").indexOf("new-attendee") > -1) {

            parentElem.find(".attendance-form").toggle('50');

        }
    };


    var toggleNewAttendee = function() {

        var parentElem = $(this).parents(".contact-list-item");

        if(parentElem.hasClass('available-attendee')) {

            parentElem.addClass('ui-selected')
                .addClass('new-attendee')
                .removeClass('available-attendee');

        }
        else if(parentElem.hasClass('new-attendee')) {

            parentElem.removeClass('ui-selected')
                .removeClass('new-attendee')
                .addClass('available-attendee')
                .find('.attendance-form').hide(200);
        }
    };

    //initializes form configuration - adds onchange functions to elements and sets default
    //behaviors and values
    var init = function() {


        //logic to add/remove contacts
        $(".contact-list-item.available-attendee .contact-list-item-name").click(toggleNewAttendee);
        $(".contact-list-item.new-attendee .contact-list-item-name").click(toggleNewAttendee);
        $(".expand-form-icon").click(showAttendanceDialog);


        highlightCurrentAttendees();
    };

    return { init            : init,
             showAllContacts : showAllContacts,
             viewAttending   : viewAttending
    };

})();

//validates the form for saving
function canSave() {

    //get the required fields
    var requiredFields = fields.filter(function(item) {
        return item.isRequired();
    });


    var canSave = true;

    requiredFields.forEach(function(item) {

        if(item.hasGoodInput()) {

            canSave = canSave && true; //don't set canSave to true if it's already false
            item.hideErrorMessage();

        }
        else {

            canSave = false;
            item.showErrorMessage();

        }
    });

    return canSave;
}

//shows the new calendar form under the calendar field
function showNewCalendarForm() {

    $("#new-calendar-group").toggle(200);

}

//ajax call to create a new calendar
function postNewCalendar() {

    var name = $("#new-calendar").val();

    var token = $("span#api_token").attr("_token");

    var calAPI = new API('calendar', token);

    var postBody = {

        'name':name
    };

    calAPI.post(postBody).then(function(result){

        var id=result['payload'].id;
        var calSelect = document.getElementById("calendar-select");

        var option = document.createElement("option");
        option.text = name;
        option.value = id;

        calSelect.add(option);
        calSelect.value = id;

        $("#new-calendar-group").hide(200);

    }, function(error) {

        alert(error.responseText);

    });
}

//general function to indication a failed AJAX call
function ajaxFailure(error) {

    var message = "Status code: " + error.status + ", Message: " + error['responseText'];

    $("#error-text").html(message)
        .parents('.alert')
        .show(200);

}

//general function to indication a successful AJAX call
function ajaxSuccess(result) {

    $("#ajax-message").html("Done!");
    window.open(getReturnUrl(), '_self');

}


//this returns an array of objects based on an existing event's existing contacts
function getUpdatedAttendances() {

    var modified = $(".current-attendee");
    var updated = [];

    for(var i = 0; i < modified.length; i++) {

        var contact = $(modified[i]);

        var refundPercentage = contact.find(".attendee-refund-percentage").val();
        var attendanceId = contact.data("attendance-id");

        var data = {
            id      : attendanceId,
            contact : contact.data("contact-id"),
            status : contact.find(".attendee-status").val(),
            notes : contact.find(".attendance-form-notes").val(),
            refund_percent : refundPercentage == 0 ? '0.00' : refundPercentage
        };

        updated.push(data);
    }

    return updated;
}

//returns an array of objects based on contacts added to a new or existing event
function getNewAttendances() {

    var newAttendees = $(".new-attendee");
    var attendances = [];

    for(var i = 0; i < newAttendees.length; i++) {

        var contact = $(newAttendees[i]);
        var refundPercentage = contact.find(".attendee-refund-percentage").val();

        var body =  {
            contact : contact.data('contact-id'),
            status : contact.find(".attendee-status").val(),
            notes : contact.find(".attendance-form-notes").val(),
            refund_percent : refundPercentage == 0? '0.00' : refundPercentage

        };
        attendances.push(body);
    }

    return attendances;
}

//generates an object suitable for a POST or a PUT to /event/{id}
function generateEventBody() {

    var length_override = 0;
    var time = moment($("#EventFrom").timepicker("getTime")).format("HH:mm:ss");
    var startDate = moment($("#EventStart").datepicker("getDate")).format("YYYY-MM-DD");

    if($("#AllDayEvent").prop("checked")) {

        length_override = parseInt($("#NumDays").val()) * 24.0 * 60.0;

    }
    else {

        length_override = parseFloat($("#EventTo").val() * 60.0);

    }


    var attendanceData = [];
    attendanceData = attendanceData.concat(getNewAttendances());
    attendanceData = attendanceData.concat(getUpdatedAttendances());

    return {
        'name' : $("#EventTitle").val(),
        'event_category': $("#category-select").val(),
        'calendar': $("#calendar-select").val(),
        'start_datetime': startDate + " " + time,
        'length_override': length_override,
        'attendances':attendanceData
    };

}

//when the user clicks Save, the form data is collected and sent to the server. If the event is a new event, then
//new contacts are recorded as Attendances. If this is an existing event, then existing contacts are sent to the
//server via a PUT

function uploadFormData(eventId, body) {

    var token = $("span#api_token").attr("_token");

    var eventBody = body == undefined ? generateEventBody() : body;
    var eventAPI = new API('event', token);

    $("#ajax-message").html('Saving...').show(200);

    //create the event
    if(!eventId) {
        eventAPI.post(eventBody).then(ajaxSuccess, ajaxFailure);

    }
    else {
        eventAPI.put(eventId, eventBody).then(ajaxSuccess, ajaxFailure);
    }

}

//TODO: This is ugly - can it be better?
function expandRecurrence() {


    var type = $("#recurrence-type").val();
    var date = moment($("#EventStart").datepicker("getDate"));
    var time = moment($("#EventFrom").timepicker("getTime"));
    var startDate = moment(date).set({'hour':time.get('hour'), 'minute':time.get('minute')});
    var original = moment(startDate);

    var dates = [];
    var interval = null;
    var intervalType = 'days';
    var endType = $("[name='recurrence-length-option']:checked").val();
    var end = null;

    //interval and interval type are used to calculate the dates
    if(type == "daily-recurrence") {
        interval = parseInt($("#daily-recurrence-value").val());
        intervalType = 'days';
        dates.push(moment(startDate).format("YYYY-MM-DD HH:mm:ss"));
    }
    else if(type == "monthly-recurrence") {
        interval = parseInt($("#monthly-recurrence-value").val());
        intervalType = 'months';
        dates.push(moment(startDate).format("YYYY-MM-DD HH:mm:ss"));
    }
    else if(type == "yearly-recurrence") {
        interval = parseInt($("#yearly-recurrence-value").val());
        intervalType = 'years';
        dates.push(moment(startDate).format("YYYY-MM-DD HH:mm:ss"));
    }
    else if(type == "weekly-recurrence" || type == "only-weekdays") {
        interval = [];
        var weekdays = moment.weekdaysShort();
        $("[name='weekday']:checked").each(function() {
            interval.push( weekdays.indexOf($(this).val()))
        });
        intervalType = 'weeks';
    }

    if(endType == 'fixed') {

        //calculate the dates by looping through INTERVAL times
        end = parseInt($("#recur-n-times").val());
        for(var i = 1; i < end; i++) {
            if(!Array.isArray(interval)) {
                var next = moment(startDate.add(interval, intervalType));
                if(next.isSameOrAfter(original)){
                    dates.push(next.format("YYYY-MM-DD HH:mm:ss"));
                }
            }
            else {
                for(var j = 0; j < interval.length; j++) {
                    var next = moment(startDate.weekday(interval[j]));
                    if(next.isSameOrAfter(original)){
                        dates.push(next.format("YYYY-MM-DD HH:mm:ss"));
                    }
                }
                //then set startDate to the beginning of next week
                var lastWeek = moment(startDate);
                startDate.add(1, 'weeks').startOf('week').hour(lastWeek.hours()).minute(lastWeek.minutes());
            }
        }
    }
    else { //calculate the dates by INTERVAL units of time, as specified by INTERVALTYPE

        end = moment($("#recur-until-date").datepicker("getDate"));
        while(startDate.isBefore(end)) {
            if(!Array.isArray(interval)) {
                var next = moment(startDate.add(interval, intervalType));
                if(next.isSameOrAfter(original)){
                    dates.push(next.format("YYYY-MM-DD HH:mm:ss"));
                }
            }
            else {
                //if INTERVAL is an array type, create a date out of the
                //individual weekdays set in INTERVAL
                for(var j = 0; j < interval.length; j++) {
                    var next = moment(startDate.weekday(interval[j]));
                    if(next.isSameOrAfter(original)){
                        dates.push(next.format("YYYY-MM-DD HH:mm:ss"));
                    }
                }
                //then set startDate to the beginning of next week
                var lastWeek = moment(startDate);
                startDate.add(1, 'weeks').startOf('week').hour(lastWeek.hours()).minute(lastWeek.minutes());
            }
        }
    }

    return dates;
}

//deletes an event if it is not an instance in a recurrence. If it is, a modal dialog is shown asking for confirmation
//the user will have the option of deleting the instance or the entire series.
function deleteEvent(eventId, groupId) {

    if(groupId == undefined || groupId == 'undefined') {

        $("#ajax-message").html("Deleting...").show();
        var token = $("span#api_token").attr("_token");

        var eventAPI = new API('event', token);
        eventAPI.delete(eventId).then( ajaxSuccess, ajaxFailure);

    } else {
        $("#myModal").modal("show");
    }
}

/*creates a recurrence by first creating the eventgrouping resource. Corresponding events and attendances are created
using the list of dates returned by expandRecurrence()
 */
function createRecurrence() {

    var token = $("span#api_token").attr("_token");
    var groupAPI = new API('eventgrouping', token);

    var body = generateEventBody();
    body['dates'] = expandRecurrence();
    body['attendances'] = getNewAttendances();

    $("#ajax-message").html("Saving...Please wait...").show();

    groupAPI.post(body).then(ajaxSuccess, ajaxFailure);
}

/* deletes a recurrence - server will delete all corresponding events and attendences. */
function deleteRecurrence(groupId) {

    var token = $("span#api_token").attr("_token");
    var groupAPI = new API('eventgrouping', token);

    groupAPI.delete(groupId).then(ajaxSuccess, ajaxFailure);
}

//Updates the recurrence with the given groupId
function updateRecurrence(groupId) {

    var token = $("span#api_token").attr("_token");
    var groupAPI = new API('eventgrouping', token);
    var newDateTime = moment(fieldsLookup['EventStart'].getValue().format("YYYY-MM-DD") + " " + fieldsLookup['EventFrom'].getValue());
    var diff = moment.duration(newDateTime - originalStartDate).asMinutes();

    //get event fields from the form
    var eventBody = generateEventBody();

    //get change in start date if any
    eventBody['date_diff'] = diff;

    //get new and updated attendances
    eventBody['attendances'] = getNewAttendances();
    eventBody['attendances'] = eventBody['attendances'].concat(getUpdatedAttendances());

    groupAPI.put(groupId, eventBody).then(ajaxSuccess, ajaxFailure);

}
/* updates the recurring event with the given eventId
 * removeFromRecurrence is a boolean. If true, event's groupId is set to 0, removing
 * it from future updates to the recurrence. Otherwise, the event is updated but is
 * left as part of the recurrence
 */
function updateRecurringEvent(eventId, removeFromRecurrence) {

    var token = $("span#api_token").attr("_token");
    var eventAPI = new API('event', token);
    var eventBody = generateEventBody();

    if(removeFromRecurrence) {
        eventBody['event_group'] = 'null';
    }

    $("#ajax-message").html('Saving...').show(200);
    eventAPI.put(eventId, eventBody).then(ajaxSuccess, ajaxFailure);
}

function getReturnUrl() {

    var paramMatches = window.location.href.match(/returnUrl=\/\S+(\/\d+)*/);
    var returnUrl = '/dashboard';

    if(paramMatches && paramMatches.length > 0) {
        returnUrl = paramMatches[0].split("=").length == 2 ? paramMatches[0].split("=")[1] : '/dashboard';
    }

    return returnUrl;
}