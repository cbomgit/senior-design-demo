<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Transformers\UserTransformer;

class APIUserController extends APIController {

    protected $transformer;

    function __construct(UserTransformer $userTransformer) {

        $this->transformer = $userTransformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = User::where('user', $request->user()->id);

        $data = $query::paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $user = User::find($id);

        if($user)
            return APIController::respond($this->transformer->transform($user));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'first' => $request->get('first'),
            'last' => $request->get('last'),
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ];

        // Validate against model
        $vlad = User::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $user = new User;
            $user->first_name = $data['first'];
            $user->last_name = $data['last'];
            $user->email = $data['email'];
            $user->password = bcrypt($data['password']);

            // TODO Remove the possible collision here
            $user->api_token = str_random(60);

            $user->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $user->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Ensure this is an authorized resource
        if($request->user()->id != $id)
            return APIController::respondUnauthorized();

        // Get the user
        $user = User::find($id);

        // Populate another array representing the new values
        $data = [];

        if($request->get('first')) {
            $data['first'] = $request->get('first');
        }

        if($request->get('last')) {
            $data['last'] = $request->get('last');
        }

        if($request->get('email')) {
            $data['email'] = $request->get('email');
        }

        if($request->get('password')) {
            $data['password'] = $request->get('password');
        }

        // Validate against model
        $vlad = User::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource
            if(array_key_exists('first', $data)) {
                $user->first_name = $data['first'];
                echo 'boop';
            }

            if(array_key_exists('last', $data)) {
                $user->last_name = $data['last'];
            }

            if(array_key_exists('email', $data)) {
                $user->email = $data['email'];
            }

            if(array_key_exists('password', $data)) {
                $user->password = bcrypt($data['password']);
            }

            $user->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $user->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $user = User::find($id);

        // Id's match, delete the specified user
        if($request->user()->id == $user->id) {

            // Delete the user
            User::where('id', $id)->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}
