<?php
/**
 * Created by PhpStorm.
 * User: colat
 * Date: 2/16/2017
 * Time: 11:27 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Auth;

class ContactListController extends Controller
{

    /*
     * Create a new controller instance.
     *
     * @return void
     */



    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {

        $user = Auth::user();

        $api_token = $user->api_token;

        $contacts = Contact::where('user', $user->id)->get();

        return view('contactlist', ['api_token' => $api_token,
                                    'contacts' => $contacts,]);

    }

}