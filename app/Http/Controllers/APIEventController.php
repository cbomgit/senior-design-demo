<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\Attendance;
use App\Token;
use App\EventCategory;
use App\Transformers\EventTransformer;

class APIEventController extends APIController {

    protected $transformer;

    protected $user;

    function __construct(EventTransformer $eventTransformer) {

        $this->transformer = $eventTransformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        $query = Event::where('user', $request->user()->id);

        // Process start & end query strings
        if($request->get('start') && $request->get('end')) {
            $query = $query->whereBetween('start_datetime', [
                $request->get('start'),
                $request->get('end'),
            ]);
        }

        // Process event_cat query string
        if($request->get('cat')) {

            $eventCats = explode(",", $request->get('cat'));

            $query->where(function($query) use($eventCats) {

                foreach($eventCats as $eventCat) {
                    $query = $query->orWhere('event_category', $eventCat);
                }

            });

        }

        // Process calendar query string
        if($request->get('cal')) {

            $calendars = explode(",", $request->get('cal'));

            $query->where(function($query) use($calendars) {

                foreach($calendars as $calendar) {
                    $query = $query->orWhere('calendar', $calendar);
                }

            });

        }

        //Process event group query string
        if($request->get('group')) {
            $groups = explode(",", $request->get('group'));
            $query->where(function($query) use ($groups) {

                foreach($groups as $group) {
                    $query = $query->orWhere('event_group', $group);
                }
            });
        }

        // Paginate the data
        $data = $query->paginate($limit);
        //$data = $query->get();
        //dd($query->toSql());

        // Create a paginated response
        //return APIController::respond($this->transformer->index($data->all()));
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show(Request $request, $id) {

        $event = Event::find($id);

        // Ensure this is an authorized resource
        if($event && $request->user()->id != $event->user)
            return APIController::respondUnauthorized();

        if($event)
            return APIController::respond($this->transformer->transform($event));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'event_category'  => $request->get('event_category'),
            'event_group'  => $request->get('event_group'),
            'start_datetime' => $request->get('start_datetime'),
            'name' => $request->get('name'),
            'calendar' => $request->get('calendar'),
            'capacity_override'  => $request->get('capacity_override'),
            'length_override'  => $request->get('length_override'),
            'token_cost_override' => $request->get('token_cost_override'),
        ];

        //dump($data);

        // Validate all data against models before creating
        $vlad = Event::validateCreate($data);

        if($vlad->passes()) {

            if($request->has('attendances')) {

                $attendances = $request->get('attendances');

                foreach($attendances as $attendance) {

                    // Validate against model
                    $vlad = Attendance::validateCreate($attendance);
                    if(!$vlad->passes()) {

                        return APIController::respondError($vlad->errors());
                    }
                }
            }

            // Good data, create the new resource
            $event = new Event;
            $event->user = $request->user()->id;
            $event->event_category = $data['event_category'];
            $event->event_group = $data['event_group'];
            $event->start_datetime = $data['start_datetime'];
            $event->name = $data['name'];
            $event->capacity_override = $data['capacity_override'];
            $event->length_override = $data['length_override'];
            $event->token_cost_override = $data['token_cost_override'];
            $event->calendar = $data['calendar'];


            $event->save();

            if($request->has('attendances')) {

                $attendances = $request->get('attendances');

                foreach($attendances as $attendance) {

                    $this->createAttendance($attendance, $event, $request);

                }
            }

            // Respond with the good news
            return APIController::respond([
                'id' => $event->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $event = Event::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $event->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('user')) {
            $data['user'] = $request->get('user');
        }

        if($request->get('event_category')) {
            $data['event_category'] = $request->get('event_category');
        }

        if($request->get('event_group')) {
            $data['event_group'] = $request->get('event_group') == 'null' ? null : $request->get('event_group');
        }

        if($request->get('start_datetime')) {
            $data['start_datetime'] = $request->get('start_datetime');
        }

        if($request->get('name')) {
            $data['name'] = $request->get('name');
        }

        if($request->get('capacity_override')) {
            $data['capacity_override'] = $request->get('capacity_override');
        }

        if($request->get('length_override')) {
            $data['length_override'] = $request->get('length_override');
        }

        if($request->get('token_cost_override')) {
            $data['token_cost_override'] = $request->get('token_cost_override');
        }

        if($request->get('calendar')) {
            $data['calendar'] = $request->get('calendar');
        }

        // Validate against model
        $vlad = Event::validateUpdate($data);

        if($vlad->passes()) {

            if($request->has('attendances')) {

                $attendances = $request->get('attendances');

                foreach($attendances as $attendance) {

                    $vlad = array_key_exists('id', $attendance) ? Attendance::validateUpdate($attendance)
                                                                : Attendance::validateCreate($attendance);
                    if(!$vlad->passes()) {
                        APIController::respondError($vlad->errors());
                    }
                }
            }

            // Good data, modify the resource
            if(array_key_exists('user', $data)) {
                $event->user = $data['user'];
            }

            if(array_key_exists('event_category', $data)) {
                $event->event_category = $data['event_category'];
            }

            if(array_key_exists('event_group', $data)) {
                $event->event_group = $data['event_group'];
            }

            if(array_key_exists('start_datetime', $data)) {
                $event->start_datetime = $data['start_datetime'];
            }

            if(array_key_exists('name', $data)) {
                $event->name = $data['name'];
            }

            if(array_key_exists('capacity_override', $data)) {
                $event->capacity_override = $data['capacity_override'];
            }

            if(array_key_exists('length_override', $data)) {
                $event->length_override = $data['length_override'];
            }

            if(array_key_exists('token_cost_override', $data)) {
                $event->token_cost_override = $data['token_cost_override'];
            }

            if(array_key_exists('calendar', $data)) {
                $event->calendar = $data['calendar'];
            }

            $event->save();

            if($request->has('attendances')) {

                $attendances = $request->get('attendances');

                foreach($attendances as $attendance) {

                    //user may pass new attendances or existing attendances to update
                    if(array_key_exists('id', $attendance)) {
                        $this->updateAttendance($attendance, $event, $request);
                    }
                    else {
                        $this->createAttendance($attendance, $event, $request);
                    }
                }
            }

            // Respond with the good news
            return APIController::respond([
                'id' => $event->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        $event = Event::find($id);

        // It's a match, delete the specified user
        if($request->user()->id == $event->user) {

                $event->delete();

                // Return the good news
                return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

    public function updateAttendance($attendance, $event, $request) {
        $a = Attendance::find($attendance['id']);
        $oldStatus = null;
        $oldRefundPercent = null;

        if(array_key_exists('contact', $attendance)) {
            $a->contact = $attendance['contact'];
        }
        if(array_key_exists('status', $attendance)) {
            $a->status = $attendance['status'];

        }
        if(array_key_exists('refund_percent', $attendance)) {
            $a->refund_percent = $attendance['refund_percent'];
        }

        if(array_key_exists('notes', $attendance)) {
            $a->notes = $attendance['notes'];
        }

        $a->save();

        //also update the token for this attendance if its status is updated
        if($oldStatus && $oldStatus == 'RESERVED' && $a->status != 'RESERVED') {

            $cat = EventCategory::find(Event::find($a->event)->event_category);
            $default_cost = $cat->default_token_cost;

            $token = Token::where('attendance', $a->id)->first();
            $token->amount = $default_cost - ($default_cost  * ($a->refund_percent/100));
            $token->pending = 0;

            $token->save();

        } //if the refund percentage was changed, update the token
        elseif ($oldRefundPercent != $a->refund_percent) {

            $cat = EventCategory::find(Event::find($a->event)->event_category);
            $default_cost = $cat->default_token_cost;

            $token = Token::where('attendance', $a->id)->first();
            $token->amount = $default_cost - ($default_cost  * ($a->refund_percent/100));

            $token->save();

        }
    }

    public function createAttendance($attendance, $event, $request) {

        $a = new Attendance;

        $a->contact = $attendance['contact'];
        $a->event = $event->id;
        $a->notes = $attendance['notes'];
        $a->status = $attendance['status'];
        $a->refund_percent = $attendance['refund_percent'];
        $a->user = $request->user()->id;

        $a->save();

        //create the token tied to this attendance
        $this->createToken($a, $event, $request);
    }

    public function createToken($attendance, $event, $request) {

        //also create a token
        $token = new Token;
        $token->attendance = $attendance->id;
        $token->event = $event->id;
        $token->event_category = $event->event_category;
        $token->contact = $attendance->contact;
        $token->pending = true;

        // Use event token cost override if set or the category's cost
        if($event->token_cost_override) {

            $token->amount = $event->token_cost_override;

        } else {

            $token->amount = EventCategory::find($event->event_category)->default_token_cost;

        }

        $token->save();

    }
}
