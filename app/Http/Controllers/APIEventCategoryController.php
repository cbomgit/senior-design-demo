<?php

namespace App\Http\Controllers;

use App\EventCategory;
use Illuminate\Http\Request;

use App\Transformers\EventCategoryTransformer;

class APIEventCategoryController extends APIController {

    protected $transformer;

    function __construct(EventCategoryTransformer $eventCategoryCategoryTransformer) {

        $this->transformer = $eventCategoryCategoryTransformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        //$data = EventCategory::paginate($limit);
        // Paginate the data
        $data = EventCategory::where('user', $request->user()->id)->paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $eventCategory = EventCategory::find($id);

        if($eventCategory)
            return APIController::respond($this->transformer->transform($eventCategory));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'name' => $request->get('name'),
            'color'  => $request->get('color'),
            'default_length'  => $request->get('default_length'),
            'default_capacity' => $request->get('default_capacity'),
            'default_token_cost' => $request->get('default_token_cost'),
        ];

        // Validate against model
        $vlad = EventCategory::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $eventCategory = new EventCategory();

            $eventCategory->user = $request->user()->id;
            $eventCategory->name = $data['name'];
            $eventCategory->color = $data['color'];
            $eventCategory->default_length = $data['default_length'];
            $eventCategory->default_capacity = $data['default_capacity'];
            $eventCategory->default_token_cost = $data['default_token_cost'];

            $eventCategory->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $eventCategory->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $eventCategory = EventCategory::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $eventCategory->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('user')) {
            $data['user'] = $request->get('user');
        }

        if($request->get('name')) {
            $data['name'] = $request->get('name');
        }

        if($request->get('color')) {
            $data['color'] = $request->get('color');
        }

        if($request->get('default_length')) {
            $data['default_length'] = $request->get('default_length');
        }

        if($request->get('default_capacity')) {
            $data['default_capacity'] = $request->get('default_capacity');
        }

        if($request->get('default_token_cost')) {
            $data['default_token_cost'] = $request->get('default_token_cost');
        }

        // Validate against model
        $vlad = EventCategory::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource
            if(array_key_exists('user', $data)) {
                $eventCategory->user = $data['user'];
            }

            if(array_key_exists('name', $data)) {
                $eventCategory->name = $data['name'];
            }

            if(array_key_exists('color', $data)) {
                $eventCategory->color = $data['color'];
            }

            if(array_key_exists('default_length', $data)) {
                $eventCategory->default_length = $data['default_length'];
            }

            //this was missing but still doesn't work. - Christian
            if(array_key_exists('default_capacity', $data)) {
                $eventCategory->default_capacity = $data['default_capacity'];
            }

            if(array_key_exists('default_token_cost', $data)) {
                $eventCategory->default_token_cost = $data['default_token_cost'];
            }

            $eventCategory->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $eventCategory->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $eventCategory = EventCategory::find($id);

        // It's a match, delete the specified event category
        if($request->user()->id == $eventCategory->user) {

            $eventCategory->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}
