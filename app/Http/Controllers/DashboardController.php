<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\EventCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $user = Auth::user();

        $calendars = Calendar::whereUser($user->id)->get();

        $categories = EventCategory::whereUser($user->id)->get();

        return view('dashboard', [
            'api_token' => $user->api_token,
            'calendars' => $calendars,
            'categories' => $categories,
        ]);

    }

}