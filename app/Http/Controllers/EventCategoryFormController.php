<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 1/29/17
 * Time: 4:29 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCategory;
use Illuminate\Support\Facades\Auth;

class EventCategoryFormController extends Controller
{

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index($eventCatId = null) {

        $user = Auth::user();

        $api_token = \Auth::user()->api_token;

        if($eventCatId) {

            // Get the event category and return errors if it doesn't exist
            $eventCat = EventCategory::find($eventCatId);

            if(!$eventCat) {
                return view('errors.404');
            }

            // Transform the color for now - can this be a hex value in the DB?
            if($eventCat->color == 'RED') {
                $eventCat->color = '#9f1414';
            }

            return view('eventcatform', [
                'id' => $eventCatId,
                'api_token' => $api_token,
                'eventCategory' => $eventCat,
            ]);

        } else {
            return view('eventcatform', [
                'api_token' => $api_token,
            ]);

        }

    }

}