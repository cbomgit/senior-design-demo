<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use Illuminate\Support\Facades\Auth;



class ContactFormController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($contactId = null)
    {
        $api_token = \Auth::user()->api_token;

        if ($contactId) {

            //get the contact and return errors if it doesn't exist
            $contact = Contact::find($contactId);
            if (!$contact) {
                return view('errors.404');
            }


            return view('contactform', ['id' => $contactId,
                                        'api_token' => $api_token,
                                        'contact' => $contact, ]);
        }
        else {
            return view('contactform', ['api_token' => $api_token]);
        }
    }


}