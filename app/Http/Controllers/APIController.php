<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 12:10 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Request;

abstract class APIController extends Controller {

    const LIMIT = 1000;

    public static function respond($payload) {

        return Response([
            'status' => 'success',
            'payload' => $payload,
        ], Response::HTTP_ACCEPTED);

    }

    public static function respondPaged($payload, $data) {

        return Response([
            'status' => 'success',
            'pagination' => [
                'current' => $data->currentPage(),
                'total' => $data->lastPage(),
            ],
            'payload' => $payload,
        ], Response::HTTP_ACCEPTED);

    }

    public static function respondError($message = 'Invalid Request.', $code = Response::HTTP_BAD_REQUEST) {

        return Response([
            'status' => 'error',
            'error' => [
                'code' => $code,
                'message' => $message,
            ],
        ], $code);

    }

    public static function respondUnauthorized() {

        return APIController::respondError('Unauthorized', Response::HTTP_UNAUTHORIZED);

    }

    public static function respondNotFound() {

        return APIController::respondError('Resource not found.', Response::HTTP_NOT_FOUND);

    }

    // Default APIs

    // Show the form for creating a new resource
    public function create() {

        return APIController::respondError('Not implemented, go away.', Response::HTTP_FORBIDDEN);

    }

    // Show the form for editing the specified resource
    public function edit($id) {

        return APIController::respondError('Not implemented, go away.', Response::HTTP_FORBIDDEN);

    }

}