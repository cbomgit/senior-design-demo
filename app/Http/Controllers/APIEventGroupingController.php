<?php

namespace App\Http\Controllers;

use App\EventGrouping;
use App\Event;
use App\Attendance;
use App\Token;
use Illuminate\Http\Request;
use App\EventCategory;

use App\Transformers\EventGroupingTransformer;
use App\Http\Controllers\APIEventController;
use \App\Transformers\EventTransformer;

class APIEventGroupingController extends APIController {

    protected $transformer;

    function __construct(EventGroupingTransformer $eventGroupingTransformer) {

        $this->transformer = $eventGroupingTransformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = EventGrouping::where('user', $request->user()->id);

        $data = $query->paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $eventGrouping = EventGrouping::find($id);

        if($eventGrouping)
            return APIController::respond($this->transformer->transform($eventGrouping));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Good data, create the new resource
        $eventGrouping = new EventGrouping();

        $eventGrouping->user = $request->user()->id;

        if(!$request->has('dates')) {

            return APIController::respondError('no event dates provided');

        }

        $dates = $request->get('dates');

        $eventData = $this->getEventFromRequest($request);

        $validation = $this->validateRequestData($request, $eventData);

        if(!$validation['passes']) {

            return APIController::respondError($validation['errors']);

        }
         //save the grouping ID and loop through each new event, setting the group id and saving each one
        $eventGrouping->save();

        foreach($dates as $date) {

            $this->createEvent($request, $eventData, $date, $eventGrouping);

        }

        // Respond with the good news
        return APIController::respond([
            'id' => $eventGrouping->id,
        ]);

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        //validate the event data
        $eventData = [];

        if($request->has('name')) {
            $eventData['name'] = $request->get('name');
        }
        if($request->has('calendar')) {
            $eventData['calendar'] = $request ->get('calendar');
        }
        if($request->has('event_category')) {
            $eventData['event_category'] = $request->get('event_category');
        }
        if($request->has('length_override')) {
            $eventData['length_override'] = $request->get('length_override');
        }

        $vlad = Event::validateUpdate($eventData);

        if(!$vlad->passes()) {
            return APIController::respondError($vlad->errors());
        }

        //validate the attendance data
        if($request->has('attendances')) {

            $attendances = $request->get('attendances');

            foreach($attendances as $a) {

                $vlad = array_key_exists('id', $a) ? Attendance::validateUpdate($a) : Attendance::validateCreate($a);

                if(!$vlad->passes()) {

                    return APIController::respondError($vlad->errors());

                }
            }
        }

        //get all events in this recurrence and update each one with new data
        $events = Event::where('event_group', $id)->get();

        foreach($events as $e) {

            if($request->has('name')) {
                $e->name = $request->get('name');
            }
            if($request->has('calendar')) {
                $e->calendar = $request->get('calendar');
            }
            if($request->has('event_category')) {
                $e->event_category = $request->get('event_category');
            }
            if($request->has('length_override')) {
                $e->length_override = $request->get('length_override');
            }
            if($request->has('date_diff') && $request->get('date_diff') != '0') {

                //get the passed in date difference and apply it to each event start_datetime
                $diff = $request->get('date_diff');

                if($diff > 0) {
                    $newTime = date('Y-m-d H:i:s', strtotime($e->start_datetime. ' + '.$diff.' minutes'));
                }
                else {
                    $diff = abs($diff);
                    $newTime = date('Y-m-d H:i:s', strtotime($e->start_datetime.' -'.$diff.' minutes'));
                }
                $e->start_datetime = $newTime;
            }

            $e->save();

            if($request->has('attendances')) {

                $attendances = $request->get('attendances');

                foreach($attendances as $a) {
                    //user may pass new attendances or existing attendances to update
                    $eventApi = new APIEventController(new EventTransformer);

                    if(array_key_exists('id', $a)) {

                        //get the matching attendance
                        $existing = Attendance::where([['event', '=', $e->id], ['contact', '=', $a['contact']]])->first();

                        //set its id
                        $a['id'] = $existing->id;

                        //update it
                        $eventApi->updateAttendance($a, $e, $request);
                    }
                    else {
                        //no id passed with this attendance, so it doesn't exist yet - create it
                        $eventApi->createAttendance($a, $e, $request);
                    }
                }
            }
        }
        return APIController::respond([
            'id' => $id
        ]);
    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $eventGrouping = EventGrouping::find($id);

        // It's a match, delete the specified event Grouping
        if($eventGrouping && $request->user()->id == $eventGrouping->user) {

            $eventGrouping->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

    private function getEventFromRequest(Request $request) {

        $eventData = [];

        $eventData['name'] = $request->get('name');
        $eventData['start_datetime'] = $request->get('start_datetime');
        $eventData['calendar'] = $request ->get('calendar');
        $eventData['event_category'] = $request->get('event_category');
        $eventData['length_override'] = $request->get('length_override');

        return $eventData;
    }

    private function validateRequestData(Request $request, $eventData) {

        $vlad = Event::validateCreate($eventData);

        if(!$vlad->passes()) {

            return [ 'passes' => false, 'errors' => $vlad->errors() ];

        }

        if($request->has('attendances')) {

            $attendances = $request->get('attendances');

            foreach($attendances as $a) {

                $vlad = Attendance::validateCreate($a);

                if(!$vlad->passes()) {

                    return [ 'passes' => false, 'errors' => $vlad->errors() ];
                }
            }
        }

        return [ 'passes' => true ];
    }

    private function createEvent(Request $request, $eventData, $date, $eventGrouping) {

        $event = new Event;

        $event->user = $request->user()->id;
        $event->event_group = $eventGrouping->id;
        $event->name = $eventData['name'];
        $event->calendar = $eventData['calendar'];
        $event->event_category = $eventData['event_category'];
        $event->length_override = $eventData['length_override'];
        $event->start_datetime = $date;

        $event->save();

        if($request->has('attendances')) {

            $eventApi = new APIEventController(new EventTransformer);
            
            $attendances = $request->get('attendances');

            foreach($attendances as $a) {

                $eventApi->createAttendance($a, $event, $request);

            }
        }
    }

}