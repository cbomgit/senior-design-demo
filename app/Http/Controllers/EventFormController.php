<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 1/29/17
 * Time: 4:29 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventCategory;
use App\Calendar;
use App\Contact;
use App\Attendance;

use Illuminate\Support\Facades\Auth;

class EventFormController extends Controller
{
    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($eventId = null)
    {
        $user = Auth::user();

        $api_token = $user->api_token;

        if($eventId) {


            //get the event and return errors if it doesn't exist or if it does not belong to the current user
            $event = Event::find($eventId);
            if(!$event) {
                return view('errors.404');
            } elseif ($event->user != $user->id) {
                return view('errors.404');
            }

            //Get info pertaining to this event
            $categories = EventCategory::where('user', $user->id)->get();

            $calendars = Calendar::where('user', $user->id)->get();

            $contacts = Contact::where('user', $user->id)->get();

            $attendances = Attendance::where('event',$eventId)->get();

            return view('eventform', [
                'id' => $eventId,
                'api_token' => $api_token,
                'event' => $event,
                'categories' => $categories,
                'calendars' => $calendars,
                'contacts' => $contacts,
                'attendances' => $attendances
            ]);

        } else {

            //Get info necessary to fill out form
            $categories = EventCategory::where('user', $user->id)->get();

            $calendars = Calendar::where('user', $user->id)->get();

            $contacts = Contact::where('user', $user->id)->get();

            return view('eventform', [
                'api_token' => $api_token,
                'categories' => $categories,
                'calendars' => $calendars,
                'contacts' => $contacts
            ]);
        }
    }
}