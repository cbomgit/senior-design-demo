<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 1/30/17
 * Time: 8:48 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventDisplayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($eventId)
    {
        return view('viewevent');
    }
}