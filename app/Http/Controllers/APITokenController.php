<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Token;
use App\Event;
use App\Contact;
use App\attendance;
use App\Transformers\TokenTransformer;

class APITokenController extends APIController {

    protected $transformer;

    function __construct(TokenTransformer $transformer) {

        $this->transformer = $transformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = Token::orderBy('pending', 'DESC')->where('contact', $request->user()->id);

        $data = $query->paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $item = Token::find($id);

        if($item)
            return APIController::respond($this->transformer->transform($item));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'contact'  => $request->get('contact'),
            'event'  => $request->get('event'),
            'attendance' => $request->get('event_category'),
            'pending'  => $request->get('pending'),
            'amount' => $request->get('amount'),
        ];

        // Validate against model
        $vlad = Token::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $item = new Token;

            if($request->get('event')) {

                $contact = Contact::find($data['contact']);

                if(!$contact or $contact->user != $request->user()->id) {
                    return APIController::respondError("Contact does not exist or doesn't belong to you.");
                }

                $item->contact = $contact->id;

            }

            if($request->get('event')) {

                $event = Event::find($data['event']);

                if(!$event or $event->user != $request->user()->id) {
                    return APIController::respondError("Event does not exist or doesn't belong to you.");
                }

                $item->event = $event->id;

                $item->event_category = $event->event_category;

            }

            if($request->get('attendance')) {

                $attendance = Attendance::find($data['attendance']);

                if(!$attendance or $attendance->contact->user != $request->user()->id) {
                    return APIController::respondError("Attendance does not exist or doesn't belong to you.");
                }

                $item->attendance = $attendance->id;

            }

            $item->pending = $data['pending'];
            $item->amount = $data['amount'];

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $item = Token::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $item->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('contact')) {
            $data['contact'] = $request->get('contact');
        }

        if($request->get('event')) {
            $data['event'] = $request->get('event');
        }

        if($request->get('attendance')) {
            $data['attendance'] = $request->get('attendance');
        }

        if($request->get('pending')) {
            $data['pending'] = $request->get('pending');
        }

        if($request->get('amount')) {
            $data['amount'] = $request->get('amount');
        }

        // Validate against model
        $vlad = Token::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource

            if(array_key_exists('contact', $data)) {

                $contact = Contact::find($data['contact']);

                if(!$contact or $contact->user != $request->user()->id) {
                    return APIController::respondError("Contact does not exist or doesn't belong to you.");
                }

                $item->contact = $contact->id;
            }

            if(array_key_exists('event', $data)) {

                $event = Event::find($data['event']);

                if(!$event or $event->user != $request->user()->id) {
                    return APIController::respondError("Event does not exist or doesn't belong to you.");
                }

                $item->event = $event->id;

                $item->event_category = $event->event_category;

            }

            if(array_key_exists('attendance', $data)) {

                $attendance = Attendance::find($data['attendance']);

                if(!$attendance or $attendance->contact->user != $request->user()->id) {
                    return APIController::respondError("Attendance does not exist or doesn't belong to you.");
                }

                $item->attendance = $attendance->id;

            }

            if(array_key_exists('pending', $data)) {
                $item->event = $data['pending'];
            }

            if(array_key_exists('amount', $data)) {
                $item->calendar = $data['amount'];
            }

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $token = Token::find($id);

        // It's a match, delete the specified user
        if($request->user()->id == $token->user) {

            // Delete the event
            $item = Token::where('id', $id);

            $item->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}