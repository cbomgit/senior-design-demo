<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 1/29/17
 * Time: 4:29 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Calendar;
use Illuminate\Support\Facades\Auth;

class CalendarListController extends Controller {
    /*
     * Create a new controller instance.
     *
     * @return void
     */
      public function __construct() {
          $this->middleware('auth');
      }

      /**
       * Show the application dashboard.
       *
       * @return \Illuminate\Http\Response
       */
      public function index() {

          $user = Auth::user();

          $api_token = $user->api_token;

          $calendars = Calendar::whereUser($user->id)->get();

          return view('calendarlist', [
              'api_token' => $api_token,
              'calendars' => $calendars,
          ]);

      }

}