<?php
/**
 * Created by PhpStorm.
 * User: colat
 * Date: 3/13/2017
 * Time: 1:03 AM
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Token;
use App\Attendance;
use App\Event;
use Illuminate\Support\Facades\Auth;

class ContactProfileController extends Controller
{

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($contactId) {

        $user = Auth::user();

        $api_token = $user->api_token;

        $thisContact = Contact::find($contactId);
        $pending = Token::where([
            ['contact', '=', $contactId],
            ['event', '!=', 'null'],
            ['pending', '=', '1']
        ])->get();

        $balance = 0;
        $complete = Token::where([
            ['contact', '=', $contactId],
            ['event', '!=', 'null'],
            ['pending', '=', '0']
        ])->get();

        $p = array();
        $c = array();
        foreach ($pending as $token) {
            $att = Attendance::find($token->attendance);
            $event = Event::find($token->event);
            $p[] = [
                'attendance' => $att,
                'token' => $token,
                'event' => $event,
            ];
        }

        foreach ($complete as $token) {
            $att = Attendance::find($token->attendance);
            $event = Event::find($token->event);
            $c[] = [
                'attendance' => $att,
                'token' => $token,
                'event' => $event,
            ];
            $balance += $token->amount;
        }

        return view('contactprofile', ['api_token' => $api_token,
                                       'contact' => $thisContact,
                                       'pending' => $p,
                                       'complete' => $c,
                                       'balance' => $balance]);

    }

}