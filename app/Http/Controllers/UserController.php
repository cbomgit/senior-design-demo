<?php
/**
 * Created by PhpStorm.
 * User: colat
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\UserProfile;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index() {

        $user = Auth::user();

        $api_token = $user->api_token;

        $users = UserProfile::where('user', $user->id)->get();

        return view('userprofile', ['api_token' => $api_token,
            'users' => $users,]);

    }
}