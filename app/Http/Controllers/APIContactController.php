<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Contact;
use App\Transformers\ContactTransformer;

class APIContactController extends APIController {

    protected $transformer;

    function __construct(ContactTransformer $transformer) {

        $this->transformer = $transformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $data = Contact::paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $item = Contact::find($id);

        if($item)
            return APIController::respond($this->transformer->transform($item));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'first_name'  => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'phone' => $request->get('phone'),
            'email' => $request->get('email'),
            'notes' => $request->get('notes'),
        ];

        //dd($data);

        // Validate against model
        $vlad = Contact::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $item = new Contact;
            $item->user = $request->user()->id;
            $item->first_name = $data['first_name'];
            $item->last_name = $data['last_name'];
            $item->phone = $data['phone'];
            $item->email = $data['email'];
            $item->notes = $data['notes'];

            $item->save();


            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $item = Contact::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $item->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('first_name')) {
            $data['first_name'] = $request->get('first_name');
        }

        if($request->get('last_name')) {
            $data['last_name'] = $request->get('last_name');
        }

        if($request->get('phone')){
            $data['phone'] = $request->get('phone');
        }

        if($request->get('email')){
            $data['email'] = $request->get('email');
        }

        if($request->get('notes')) {
            $data['notes'] = $request->get('notes');
        }

        // Validate against model
        $vlad = Contact::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource
            if(array_key_exists('first_name', $data)) {
                $item->first_name = $data['first_name'];
            }

            if(array_key_exists('last_name', $data)) {
                $item->last_name = $data['last_name'];
            }

            if(array_key_exists('phone', $data)) {
                $item->phone = $data['phone'];
            }

            if(array_key_exists('email', $data)) {
                $item->email = $data['email'];
            }

            if(array_key_exists('notes', $data)) {
                $item->notes = $data['notes'];
            }

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $contact = Contact::find($id);

        // It's a match, delete the specified user
        if($request->user()->id == $contact->user) {

            // Delete the event
            $item = Contact::where('id', $id);

            $item->delete();

                // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}