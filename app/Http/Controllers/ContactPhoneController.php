<?php
/**
 * Created by PhpStorm.
 * User: colat
 * Date: 2/23/2017
 * Time: 4:45 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactPhone;
use Illuminate\Support\Facades\Auth;

class ContactPhoneController extends Controller {

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($contactphoneId = null) {

        $user = Auth::user();

        $api_token = $user->api_token;

        if($contactphoneId) {

            // Get the contact's phone number and return errors if it doesn't exist
            $phone = ContactPhone::find($contactphoneId);

            if (!$phone) {
                return view('errors.404');
            }


            return view('contactphone', ['id' => $contactphoneId,
                                        'api_token' => $api_token,
                                        'contactphone' => $phone, ]);

        }
        else {
            return view('contactphone', ['api_token' => $api_token,]);

        }

    }

}