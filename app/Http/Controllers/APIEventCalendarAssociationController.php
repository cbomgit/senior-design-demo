<?php

/*namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\EventCalendarAssociation;
use App\Transformers\EventCalendarAssociationTransformer;

class APIEventCalendarAssociationController extends APIController {

    protected $transformer;

    function __construct(EventCalendarAssociationTransformer $transformer) {

        $this->transformer = $transformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = EventCalendarAssociation::where('user', $request->user()->id);

        $data = $query::paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $item = EventCalendarAssociation::find($id);

        if($item)
            return APIController::respond($this->transformer->transform($item));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'event'  => $request->get('event'),
            'calendar' => $request->get('calendar'),
        ];

        // Validate against model
        $vlad = EventCalendarAssociation::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $item = new EventCalendarAssociation;
            $item->user = $request->user()->id;
            $item->event = $data['event'];
            $item->calendar = $data['calendar'];

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $item = EventCalendarAssociation::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $item->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('event')) {
            $data['event'] = $request->get('event');
        }

        if($request->get('calendar')) {
            $data['calendar'] = $request->get('calendar');
        }

        // Validate against model
        $vlad = EventCalendarAssociation::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource
            if(array_key_exists('event', $data)) {
                $item->event = $data['event'];
            }

            if(array_key_exists('calendar', $data)) {
                $item->calendar = $data['calendar'];
            }

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $eventCalendarAssociation = EventCalendarAssociation::find($id);

        // It's a match, delete the specified user
        if($request->user()->id == $eventCalendarAssociation->user) {

            // Delete the event
            $item = EventCalendarAssociation::where('id', $id);

            if($item->user == $id) {

                $item->delete();

                // Return the good news
                return APIController::respond(['id' => $id]);

            }

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}*/