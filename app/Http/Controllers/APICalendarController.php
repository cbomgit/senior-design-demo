<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Calendar;
use App\Transformers\CalendarTransformer;

class APICalendarController extends APIController {

    protected $transformer;

    function __construct(CalendarTransformer $calendarTransformer) {

        $this->transformer = $calendarTransformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = Calendar::where('user', $request->user()->id);

        $data = $query::paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $calendar = Calendar::find($id);

        if($calendar)
            return APIController::respond($this->transformer->transform($calendar));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'name' => $request->get('name'),
        ];

        //dump($data);

        // Validate against model
        $vlad = Calendar::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $calendar = new Calendar;
            $calendar->user = $request->user()->id;
            $calendar->name = $data['name'];

            $calendar->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $calendar->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $calendar = Calendar::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $calendar->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('name')) {
            $data['name'] = $request->get('name');
        }

        // Validate against model
        $vlad = Calendar::validateUpdate($data);

        if($vlad->passes()) {

            if(array_key_exists('name', $data)) {
                $calendar->name = $data['name'];
            }

            $calendar->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $calendar->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $calendar = Calendar::find($id);

        // It's a match, delete the specified user
        if($request->user()->id == $calendar->user) {

            $calendar->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}
