<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 1/29/17
 * Time: 4:29 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Calendar;
use Illuminate\Support\Facades\Auth;

class CalendarFormController extends Controller {

    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */
    public function index($calendarId = null) {

        $user = Auth::user();

        $api_token = $user->api_token;

        if($calendarId) {

            // Get the event category and return errors if it doesn't exist
            $calendar = Calendar::find($calendarId);

            if(!$calendar) {
                return view('errors.404');
            }

            // Transform the color for now - can this be a hex value in the DB?
            if($calendar->color == 'RED') {
                $calendar->color = '#9f1414';
            }

            return view('calendarform', [
                'id' => $calendarId,
                'api_token' => $api_token,
                'calendar' => $calendar,
            ]);

        } else {
            return view('calendarform', [
                'api_token' => $api_token,
            ]);

        }

    }

}