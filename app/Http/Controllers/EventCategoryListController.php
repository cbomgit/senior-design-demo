<?php
/**
 * Created by PhpStorm.
 * User: christian
 * Date: 1/29/17
 * Time: 4:29 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventCategory;
use Illuminate\Support\Facades\Auth;

class EventCategoryListController extends Controller {
    /*
     * Create a new controller instance.
     *
     * @return void
     */
      public function __construct()
      {
          $this->middleware('auth');
      }

      /**
       * Show the application dashboard.
       *
       * @return \Illuminate\Http\Response
       */
      public function index() {

          $user = Auth::user();

          $api_token = $user->api_token;

          $categories = EventCategory::where('user', $user->id)->get();

          return view('eventcategorylist', [
              'api_token' => $api_token,
              'categories' => $categories,
          ]);

      }

}