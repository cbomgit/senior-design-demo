<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Http\Request;

use App\Attendance;
use App\Token;
use App\Event;
use App\EventCategory;
use App\Transformers\AttendanceTransformer;

class APIAttendanceController extends APIController {

    protected $transformer;

    function __construct(AttendanceTransformer $transformer) {

        $this->transformer = $transformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = Attendance::where('user', $request->user()->id);

        $data = $query->paginate();
        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $item = Attendance::find($id);

        if($item)
            return APIController::respond($this->transformer->transform($item));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'event'   => $request->get('event'),
            'contact' => $request->get('contact'),
            'status'  => $request->get('status'),
            'refund_percent' => $request->get('refund_percent'),
            'notes' => $request->get('notes'),
        ];

        // Validate against model
        $vlad = Attendance::validateCreate($data);

        if($vlad->passes()) {

            $event = Event::find($data['event']);

            if(!$event or $event->user != $request->user()->id) {
                return APIController::respondError("Event does not exist or doesn't belong to you.");
            }

            $contact = Contact::find($data['contact']);

            if(!$contact or $contact->user != $request->user()->id) {
                return APIController::respondError("Contact does not exist or doesn't belong to you.");
            }

            // Good data, create the new resource
            $item = new Attendance;
            $item->user = $request->user()->id;
            $item->event = $event->id;
            $item->contact = $data['contact'];
            $item->status = $data['status'];
            $item->refund_percent = $data['refund_percent'];
            $item->notes = $data['notes'];

            $item->save();

            // Also create a token for this attendance
            $token = new Token;
            $token->contact = $data['contact'];
            $token->event = $data['event'];
            $token->event_category = $event->event_category;
            $token->attendance = $item->id;
            $token->pending = true;

            // Use event token cost override if set or the category's cost
            if($event->token_cost_override) {
                $token->amount = $event->token_cost_override;
            } else {
                $token->amount = EventCategory::find($event->event_category)->default_token_cost;
            }

            $token->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $item = Attendance::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $item->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];
        $oldStatus = null;
        $oldRefund = 0;
        if($request->get('status')) {
            $data['status'] = $request->get('status');
        }

        if($request->get('refund_percent')) {
            $data['refund_percent'] = $request->get('refund_percent');
        }

        if($request->get('notes')) {
            $data['notes'] = $request->get('notes');
        }

        // Validate against model
        $vlad = Attendance::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource
            if(array_key_exists('status', $data)) {
                $oldStatus = $item->status;
                $item->status = $data['status'];
            }

            if(array_key_exists('refund_percent', $data)) {
                $oldRefund = $item->refund_percent;
                $item->refund_percent = $data['refund_percent'];
            }

            if(array_key_exists('notes', $data)) {
                $item->notes = $data['notes'];
            }

            $item->save();

            //also update the token for this attendance if its status is updated
            if($oldStatus && $oldStatus == 'RESERVED' && $item->status != 'RESERVED') {
                $cat = EventCategory::find(Event::find($item->event)->event_category);
                $default_cost = $cat->default_token_cost;
                $token = Token::where('attendance', $item->id)->first();
                $token->amount = $default_cost - ($default_cost  * ($item->refund_percent/100));
                $token->pending = 0;
                $token->save();
            } //if the refund percentage was changed, update the token
            elseif ($oldRefund != $item->refund_percent) {
                $cat = EventCategory::find(Event::find($item->event)->event_category);
                $default_cost = $cat->default_token_cost;
                $token = Token::where('attendance', $item->id)->first();
                $token->amount = $default_cost - ($default_cost  * ($item->refund_percent/100));
                $token->save();
            }


            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // It's a match, delete the specified user

        $attendance = Attendance::find($id);

        // It's a match, delete the specified $attendance
        if($request->user()->id == $attendance->user) {

            // Delete the token first
            // (because the database will automatically NULL the FK)
            Token::whereAttendance($id)->delete();

            // Delete the $attendance
            $attendance->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);

        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}