<?php
/*
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ContactEmail;
use App\Transformers\ContactEmailTransformer;

class APIContactEmailController extends APIController {

    protected $transformer;

    function __construct(ContactEmailTransformer $transformer) {

        $this->transformer = $transformer;

    }

    // Display a listing of the resource
    public function index(Request $request) {

        // Get the limit query string or the default limit
        $limit = min([$request->input('limit'), APIController::LIMIT]);

        // Paginate the data
        $query = ContactEmail::where('user', $request->user()->id);

        $data = $query::paginate($limit);

        // Create a paginated response
        return APIController::respondPaged($this->transformer->index($data->all()), $data);

    }

    // Display the specified resource
    public function show($id) {

        $item = ContactEmail::find($id);

        if($item)
            return APIController::respond($this->transformer->transform($item));

        // Resource not found
        return APIController::respondNotFound();

    }

    // Store a newly created resource in storage
    public function store(Request $request) {

        // Gather data from request
        $data = [
            'contact'  => $request->get('contact'),
            'label'  => $request->get('label'),
            'phone' => $request->get('phone'),
        ];

        // Validate against model
        $vlad = ContactEmail::validateCreate($data);

        if($vlad->passes()) {

            // Good data, create the new resource
            $item = new ContactEmail;
            $item->contact = $data['contact'];
            $item->label = $data['label'];
            $item->email = $data['email'];

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Update the specified resource in storage
    public function update(Request $request, $id) {

        // Get the user
        $item = ContactEmail::find($id);

        // Ensure this is an authorized resource
        if($request->user()->id != $item->user)
            return APIController::respondUnauthorized();

        // Populate another array representing the new values
        $data = [];

        if($request->get('label')) {
            $data['label'] = $request->get('label');
        }

        if($request->get('email')) {
            $data['email'] = $request->get('email');
        }

        // Validate against model
        $vlad = ContactEmail::validateUpdate($data);

        if($vlad->passes()) {

            // Good data, modify the resource
            if(array_key_exists('label', $data)) {
                $item->label = $data['label'];
            }

            if(array_key_exists('email', $data)) {
                $item->email = $data['email'];
            }

            $item->save();

            // Respond with the good news
            return APIController::respond([
                'id' => $item->id,
            ]);

        }

        // Bad data, respond with error
        return APIController::respondError($vlad->errors());

    }

    // Remove the specified resource from storage
    public function destroy(Request $request, $id) {

        // Get the user
        $contactEmail = ContactEmail::find($id);

        // It's a match, delete the specified user
        if($request->user()->id == $contactEmail->user) {

            // Delete the event
            $item = ContactEmail::where('id', $id);

            $item->delete();

            // Return the good news
            return APIController::respond(['id' => $id]);



        }

        // Id's don't match, return an error
        return APIController::respondUnauthorized();

    }

}*/