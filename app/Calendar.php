<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Calendar
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Calendar whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calendar whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calendar whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calendar whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Calendar whereUpdatedAt($value)
 */
class Calendar extends Model
{
    protected $fillable = [
        'user',
        'default',
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [
            'name'  => 'required|min:3|max:35',
        ]);

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [
            'name'  => 'min:3|max:35',
        ]);

    }
}
