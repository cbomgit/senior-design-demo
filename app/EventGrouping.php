<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\EventGrouping
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\EventGrouping whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventGrouping whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventGrouping whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventGrouping whereUpdatedAt($value)
 */
class EventGrouping extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [

        ]);

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [

        ]);

    }

}
