<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Contact
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereUpdatedAt($value)
 * @property string $notes
 * @method static \Illuminate\Database\Query\Builder|\App\Contact wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereNotes($value)
 */
class Contact extends Model
{
    protected $fillable = [
        'user',
        'first_name',
        'last_name',
        'phone',
        'email',
        'notes'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [
            'first_name'  => 'required|min:3|max:35',
            'last_name'  => 'required|min:3|max:35',
            'phone' => 'min:3|numeric',
            'email' => 'email',
            'notes' => 'max:4000',
        ]);

    }

    public static function validateUpdate($data)
    {

        return Validator::make($data, [
            'first_name' => 'min:3|max:35',
            'last_name' => 'min:3|max:35',
            'phone' => 'required|min:10|numeric',
            'email' => 'email',
            'notes' => 'max:4000',
        ]);
    }

}
