<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\EventCategory
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property integer $default_length
 * @property integer $default_capacity
 * @property integer $default_token_cost
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereDefaultLength($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereDefaultCapacity($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereDefaultTokenCost($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereUpdatedAt($value)
 * @property integer $user
 * @method static \Illuminate\Database\Query\Builder|\App\EventCategory whereUser($value)
 */
class EventCategory extends Model {

    protected $fillable = [
        'user',
        'name',
        'color',
        'default_length',
        'default_capacity',
        'default_token_cost',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [
            'name' => 'required|min:3|max:35',
            'color' => 'required|min:6|max:6',
            'default_length' => 'required|integer',
            'default_capacity' => 'required|integer',
            'default_token_cost' => 'required|numeric',
        ]);

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [
            'name' => 'min:3|max:35',
            'color' => 'min:6|max:6',
            'default_length' => 'integer',
            'default_capacity' => 'integer',
            'default_token_cost' => 'numeric',
        ]);

    }

}
