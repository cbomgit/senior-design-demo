<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Token;
use App\Event;
use App\Attendance;

class AttendanceCommandProcess extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'attendance:process';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        echo(date("m-d-y H:i:s", time()) . ": Running attendance batch processor.\r\n");

        // Paginate the data
        $query = Token::where('pending', '=', true);

        $tokens = $query->get();

        foreach($tokens as $token) {

            if($token->event) {

                if(time() - strtotime(Event::find($token->event)->start_datetime) >= 0) {

                    $token->pending = false;
                    $token->save();

                    $att = Attendance::find($token->attendance);
                    $att->status = 'ATTENDED';
                    $att->save();

                    echo("T " . $token->id . " marked as no longer pending.\r\n");
                    echo("Att ". $att->id . " marked as ATTENDED.\r\n");
                }

            }

        }

        echo(date("m-d-y H:i:s", time()) . ": Attendance batch process complete.\r\n");

    }
}
