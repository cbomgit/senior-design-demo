<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Token
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property integer $event
 * @property integer $event_category
 * @property boolean $pending
 * @property integer $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereEventCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token wherePending($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereUpdatedAt($value)
 * @property integer $contact
 * @property integer $attendance
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereContact($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Token whereAttendance($value)
 */
class Token extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contact',
        'event',
        'event_category',
        'pending',
        'amount',
        'attendance',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [
            'contact'  => 'required|integer',
            'event'  => 'required|integer',
            'attendance' => 'required|integer',
            'pending' => 'required|alpha|min:3|max:35',
            'amount'  => 'required|alpha|min:3|max:35',
        ]);

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [
            'contact'  => 'integer',
            'event'  => 'integer',
            'attendance' => 'integer',
            'pending' => 'alpha|min:3|max:35',
            'amount'  => 'alpha|min:3|max:35',
        ]);

    }

}
