<?php

/*namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\EventCalendarAssociation
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $event
 * @property integer $calendar
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\EventCalendarAssociation whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCalendarAssociation whereEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCalendarAssociation whereCalendar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCalendarAssociation whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\EventCalendarAssociation whereUpdatedAt($value)
 */
/*class EventCalendarAssociation extends Model {

    protected $fillable = [
        'user',
        'event',
        'calendar',
    ];

    public static function validateCreate($data) {

        return Validator::make($data, [
            'event'  => 'required|integer',
            'calendar'  => 'required|integer',
        ]);

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [
            'event'  => 'integer',
            'calendar'  => 'integer',
        ]);

    }

}*/