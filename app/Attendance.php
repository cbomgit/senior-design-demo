<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Attendance
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property integer $event
 * @property integer $contact
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereEvent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereContact($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereUpdatedAt($value)
 * @property string $status
 * @property float $refund_percent
 * @property string $notes
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereRefundPercent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Attendance whereNotes($value)
 */
class Attendance extends Model {

    protected $fillable = [
        'user',
        'event',
        'calendar',
        'contact',
        'status',
        'refund_percent',
        'notes',
    ];

    public static function validateCreate($data) {

        return Validator::make($data, [
            'status' => 'required|min:3|max:35',
            'refund_percent' => 'between:0,100.0',
            'notes' => 'required|max:4000'
        ]);

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [
            'status' => 'min:3|max:35',
            'refund_percent' => 'between:0,100.0',
            'notes' => 'required|max:4000'
        ]);

    }

}
