<?php
/**
 * Created by PhpStorm.
 * User: colat
 * Date: 2/16/2017
 * Time: 1:41 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Contact
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property string $first_name
 * @property string $last_name
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Contact whereUpdatedAt($value)
 */
class ContactList extends Model
{
    protected $fillable = [
        'user',
        'first_name',
        'last_name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [
            'first_name'  => 'required|min:3|max:35',
            'last_name'  => 'required|min:3|max:35',
        ]);

    }

    public static function validateUpdate($data)
    {

        return Validator::make($data, [
            'first_name' => 'min:3|max:35',
            'last_name' => 'min:3|max:35',
        ]);
    }

}
