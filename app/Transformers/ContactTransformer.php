<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class ContactTransformer extends Transformer {

    protected function transformItem($contact) {

        return [
            'id' => (int) $contact['id'],
            'first_name' => $contact['first_name'],
            'last_name' => $contact['last_name'],
            'notes' => $contact['notes'],
        ];

    }

    protected function indexItem($contact) {

        return [
            'id' => (int) $contact['id'],
            'notes' => $contact['notes'],
            'first_name' => $contact['first_name'],
            'last_name' => $contact['last_name'],
        ];

    }

}