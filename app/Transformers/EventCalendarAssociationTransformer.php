<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class EventCalendarAssociationTransformer extends Transformer {

    protected function transformItem($eventCalAssoc) {

        return [
            'event' => (int) $eventCalAssoc['event'],
            'calendar' => (int) $eventCalAssoc['calendar'],
        ];

    }

    protected function indexItem($eventCalAssoc) {

        return [
            'event' => (int) $eventCalAssoc['event'],
            'calendar' => (int) $eventCalAssoc['calendar'],
        ];

    }

}