<?php namespace App\Transformers;

/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:48 AM
 */
abstract class Transformer {

    public function transform($items) {

        if(is_array($items))
            return array_map([$this, 'transform'], $items);
        else
            return $this::transformItem($items);



    }

    public function index($items) {

        if(is_array($items))
            return array_map([$this, 'index'], $items);
        else
            return $this::indexItem($items);

    }

    protected abstract function transformItem($item);

    protected abstract function indexItem($item);

}