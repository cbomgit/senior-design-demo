<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class TokenTransformer extends Transformer {

    protected function transformItem($token) {

        return [
            'id' => (int) $token['id'],
            'contact' => $token['contact'],
            'event' => (int) $token['event'],
            'event_category' => $token['event_category'],
            'attendance' => $token['attendance'],
            'pending' => (boolean) $token['pending'],
            'amount' => (double) $token['amount'],
            'created_at' => $token['created_at'],
            'updated_at' => $token['updated_at'],
        ];

    }

    protected function indexItem($token) {

        return [
            'id' => (int) $token['id'],
            'contact' => $token['contact'],
            'pending' => (boolean) $token['pending'],
            'amount' => (double) $token['amount'],
        ];

    }

}