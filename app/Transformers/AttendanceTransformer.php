<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class AttendanceTransformer extends Transformer {

    protected function transformItem($attendance) {

        return [
            'id' => (int) $attendance['id'],
            'user' => (int) $attendance['user'],
            'event' => (int) $attendance['event'],
            'contact' => (int) $attendance['contact'],
            'status' => $attendance['status'],
            'refund_percent' => $attendance['refund_percent'],
            'notes' => $attendance['notes'],
            'created_at' => $attendance['created_at'],
            'updated_at' => $attendance['updated_at'],
        ];

    }

    protected function indexItem($attendance) {

        return [
            'id' => (int) $attendance['id'],
            'user' => (int) $attendance['user'],
            'event' => (int) $attendance['event'],
            'contact' => (int) $attendance['contact'],
            'status' => $attendance['status'],
            'refund_percent' => $attendance['refund_percent'],
        ];

    }

}