<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class EventCategoryTransformer extends Transformer {

    protected function transformItem($event) {

        return [
            'id' => (int) $event['id'],
            'name' => $event['name'],
            'color' => $event['color'],
            'default_length' => $event['default_length'],
            'default_capacity' => $event['default_capacity'],
            'default_token_cost' => $event['default_token_cost'],
        ];

    }

    protected function indexItem($event) {

        return [
            'id' => (int) $event['id'],
            'name' => $event['name'],
            'color' => $event['color'],
            'default_length'  => $event['default_length'],
            'default_capacity'  => $event['default_capacity'],
            'default_token_cost' => $event['default_token_cost'],
        ];

    }

}