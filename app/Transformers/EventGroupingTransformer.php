<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class EventGroupingTransformer extends Transformer {

    protected function transformItem($event) {

        return [
            'id' => (int) $event['id'],
        ];

    }

    protected function indexItem($event) {

        return [
            'id' => (int) $event['id'],
        ];

    }

}