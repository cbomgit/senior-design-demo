<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class EventTransformer extends Transformer {

    protected function transformItem($event) {

        return [
            'id' => (int) $event['id'],
            'user' => (int) $event['user'],
            'name' => $event['name'],
            'event_category'  => (int) $event['event_category'],
            'event_category_name' => $event['EventCategoryName'],
            'event_category_color' => $event['EventCategoryColor'],
            'event_group'  => (int) $event['event_group'],
            'start_datetime' => $event['start_datetime'],
            'color_override' => $event['color_override'],
            'capacity_override'  => $event['capacity_override'],
            'length_override'  => $event['length_override'],
            'token_cost_override' => $event['token_cost_override'],
        ];

    }

    protected function indexItem($event) {

        return [
            'id' => (int) $event['id'],
            'user' => $event['user'],
            'name' => $event['name'],
            'event_category'  => $event['event_category'],
            'event_category_name' => $event['EventCategoryName'],
            'event_category_color' => $event['EventCategoryColor'],
            'event_group'  => $event['event_group'],
            'start_datetime' => $event['start_datetime'],
            'color_override' => $event['color_override'],
            'capacity_override'  => $event['capacity_override'],
            'length_override'  => $event['length_override'],
            'token_cost_override' => $event['token_cost_override'],
        ];

    }

}