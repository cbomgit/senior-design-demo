<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class ContactEmailTransformer extends Transformer {

    protected function transformItem($contactEmail) {

        return [
            'id' => (int) $contactEmail['id'],
            'contact' => (int) $contactEmail['contact'],
            'label' => $contactEmail['label'],
            'email' => $contactEmail['email'],
            'created_at' => $contactEmail['created_at'],
            'updated_at' => $contactEmail['updated_at'],
        ];

    }

    protected function indexItem($contactEmail) {

        return [
            'id' => (int) $contactEmail['id'],
            'contact' => (int) $contactEmail['contact'],
            'label' => $contactEmail['label'],
            'email' => $contactEmail['email'],
        ];

    }

}