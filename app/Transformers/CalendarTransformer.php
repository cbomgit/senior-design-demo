<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class CalendarTransformer extends Transformer {

    protected function transformItem($calendar) {

        return [
            'id' => (int) $calendar['id'],
            'user' => (int) $calendar['user'],
            'default' => (boolean) $calendar['default'],
            'name' => $calendar['name'],
            'created_at' => $calendar['created_at'],
            'updated_at' => $calendar['updated_at'],
        ];

    }

    protected function indexItem($calendar) {

        return [
            'id' => (int) $calendar['id'],
            'default' => (boolean) $calendar['default'],
            'name' => $calendar['name'],
        ];

    }

}