<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class UserTransformer extends Transformer {

    protected function transformItem($user) {

        return [
            'id' => (int) $user['id'],
            'first' => $user['first_name'],
            'last' => $user['last_name'],
            'email' => $user['email'],
        ];

    }

    protected function indexItem($user) {

        return [
            'id' => (int) $user['id'],
            'first' => $user['first_name'],
            'last' => $user['last_name'],
        ];

    }

}