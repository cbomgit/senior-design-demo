<?php
/**
 * Created by PhpStorm.
 * User: Cody
 * Date: 10/16/2016
 * Time: 11:51 AM
 */

namespace App\Transformers;

class ContactPhoneTransformer extends Transformer {

    protected function transformItem($contactPhone) {

        return [
            'id' => (int) $contactPhone['id'],
            'contact' => (int) $contactPhone['contact'],
            'label' => $contactPhone['label'],
            'phone' => $contactPhone['phone'],
            'created_at' => $contactPhone['created_at'],
            'updated_at' => $contactPhone['updated_at'],
        ];

    }

    protected function indexItem($contactPhone) {

        return [
            'id' => (int) $contactPhone['id'],
            'contact' => (int) $contactPhone['contact'],
            'label' => $contactPhone['label'],
            'phone' => $contactPhone['phone'],
        ];

    }

}