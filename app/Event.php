<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Validator;

/**
 * App\Event
 *
 * @mixin \Eloquent
 * @property integer $id
 * @property integer $user
 * @property string $name
 * @property integer $event_category
 * @property integer $event_group
 * @property string $start_datetime
 * @property string $color_override
 * @property integer $capacity_override
 * @property integer $length_override
 * @property integer $token_cost_override
 * @property integer $calendar
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereUser($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereEventCategory($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereEventGroup($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereStartDatetime($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereColorOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereCapacityOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereLengthOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereTokenCostOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereUpdatedAt($value)
 * @property-read mixed $event_category_name
 * @property-read mixed $event_category_color
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereCalendar($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Event whereName($value)
 */
class Event extends Model
{
    protected $fillable = [
        'user',
        'event_category',
        'event_group',
        'start_datetime',
        'name',
        'capacity_override',
        'length_override',
        'token_cost_override',
        'calendar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * https://laravel.com/docs/5.3/validation#available-validation-rules
     * @param $data
     * @return \Illuminate\Validation\Validator
     */
    public static function validateCreate($data) {

        return Validator::make($data, [
            'event_category'  => 'required|integer',
            'event_group'  => 'nullable|integer',
            'start_datetime' => 'required|date',
            'name' => 'required|min:3|max:35',
            'capacity_override'  => 'nullable|integer',
            'length_override'  => 'nullable|integer',
            'token_cost_override' => 'nullable|numeric',
            'calendar' => 'nullable|numeric'
        ]);

    }

    protected $appends = [
        'EventCategoryName',
        'EventCategoryColor'
    ];

    // Hack to add the event cat name to the JSON output
    public function getEventCategoryNameAttribute()
    {
        //return $this->belongsTo('\App\EventCategory', 'event_category')->name;
        return EventCategory::find($this->event_category)->name;
    }

    public function getEventCategoryColorAttribute()
    {
        //return $this->belongsTo('\App\EventCategory', 'event_category')->name;
        return EventCategory::find($this->event_category)->color;
    }

    public function event_category_name()
    {

    }

    public static function validateUpdate($data) {

        return Validator::make($data, [
            'event_category'  => 'integer',
            'event_group'  => 'integer|nullable',
            'start_datetime' => 'date',
            'name' => 'min:3|max:35',
            'capacity_override'  => 'integer',
            'length_override'  => 'integer',
            'token_cost_override' => 'numeric',
            'calendar' => 'numeric',
        ]);

    }
}
