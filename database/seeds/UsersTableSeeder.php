<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
            'first_name' => 'Cody',
            'last_name' => 'Smith',
            'email' => 'smartkid101@gmail.com',
            'password' => bcrypt('reasreas'),
            'api_token' => str_random(60)
        ]);
    }
}
