<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this token transaction to a particular user
            $table
                ->integer('contact')
                ->unsigned();

            $table
                ->foreign('contact')
                ->references('id')->on('contacts')
                ->onDelete('cascade');

            # Tie this token to a particular event
            $table
                ->integer('event')
                ->nullable()
                ->unsigned();

            $table
                ->foreign('event')
                ->references('id')->on('events')
                ->onDelete('set null');

            # Tie this token to a particular event category
            $table
                ->integer('event_category')
                ->unsigned();
            $table
                ->foreign('event_category')
                ->references('id')->on('event_categories')
                ->onDelete('cascade');

            # Tie this token to a particular attendance
            $table
                ->integer('attendance')
                ->nullable()
                ->unsigned();

            $table
                ->foreign('attendance')
                ->references('id')->on('attendances')
                ->onDelete('set null');

            $table
                ->boolean('pending');

            $table
                ->decimal('amount');

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokens');
    }
}
