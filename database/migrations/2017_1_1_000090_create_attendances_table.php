<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this attendance to a particular user
            $table
                ->integer('user')
                ->unsigned();

            $table
                ->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            # Tie this attendance to a particular event
            $table
                ->integer('event')
                ->unsigned();

            $table
                ->foreign('event')
                ->references('id')->on('events')
                ->onDelete('cascade');

            # Link this attendance to a particular event
//            $table
//                ->integer('rescheduled_event')
//                ->unsigned();
//
//            $table
//                ->foreign('rescheduled_event')
//                ->references('id')->on('events')
//                ->nullable()
//                ->onDelete('set null');

            $table
                ->enum('status', [
                    'RESERVED',
                    'ATTENDED',
                    'ATTENDED_REFUNDED',
                    'ATTENDED_PARTIAL_REFUND',
                    'CANCLED_REFUNDED',
                    'CANCLED_NOT_REFUNDED',
                    'CANCLED_PARTIAL_REFUND',
                    'RESCHEDULED_REFUNDED',
                    'RESCHEDULED_NOT_REFUNDED',
                    'RESCHEDULED_PARTIAL_REFUND',
                ]);



            $table
                ->decimal('refund_percent')
                ->nullable();

            $table
                ->string('notes', 4000);

            # Tie this attendance to a particular contact
            $table
                ->integer('contact')
                ->unsigned();

            $table
                ->foreign('contact')
                ->references('id')->on('contacts')
                ->onDelete('cascade');



            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendances');
    }
}
