<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this event to a particular user
            $table
                ->integer('user')
                ->unsigned();

            $table
                ->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            # Tie this event to a particular calendar
            $table
                ->integer('calendar')
                ->unsigned()
                ->nullable();

            $table
                ->foreign('calendar')
                ->references('id')->on('calendars')
                ->onDelete('cascade');

            # Tie this event to a particular event category
            $table
                ->integer('event_category')
                ->unsigned()
                ->nullable();

            $table
                ->foreign('event_category')
                ->references('id')->on('event_categories')
                ->onDelete('cascade');

            # Tie this event to a particular event grouping
            $table
                ->integer('event_group')
                ->unsigned()
                ->nullable();

            $table
                ->foreign('event_group')
                ->references('id')->on('event_groupings')
                ->onDelete('cascade');

            $table
                ->dateTime('start_datetime');

            $table
                ->string('name', 35);

            # See http://gamedev.stackexchange.com/questions/46463/how-can-i-find-an-optimum-set-of-colors-for-10-players
            /*$table
                ->enum('color_override', [
                    'RED',
                    'DARK_ORANGE',
                    'LIGHT_ORANGE',
                    'PUKE_GREEN',
                    'NUCLEAR_GREEN',
                    'CYAN',
                    'SKY_BLUE',
                    'DEEP_BLUE',
                    'PURPLE',
                    'PINK'
                ])
                ->nullable();*/

            $table
                ->smallInteger('capacity_override')
                ->unsigned()
                ->nullable();

            $table
                ->smallInteger('length_override')
                ->unsigned()
                ->nullable();

            $table
                ->decimal('token_cost_override')
                ->nullable();

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
