<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {

            $table
                ->increments('id');
            $table
                ->string('first_name', 35);
            $table
                ->string('last_name', 35);
            $table
                ->string('email', 254)
                ->unique();
            $table
                ->string('password', 255);

            # API Token
            # See: https://gistlog.co/JacobBennett/090369fbab0b31130b51
            $table
                ->string('api_token', 60)
                ->unique();

            $table
                ->rememberToken();
            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
