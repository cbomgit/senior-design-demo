<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_emails', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this contact email to a particular contact
            $table
                ->integer('contact')
                ->unsigned();

            $table
                ->foreign('contact')
                ->references('id')
                ->on('contacts')
                ->onDelete('cascade');

            $table
                ->string('label', 20);

            $table
                ->string('email', 254);

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_emails');
    }
}
