<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventGroupingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_groupings', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this event grouping to a particular user
            $table
                ->integer('user')
                ->unsigned();

            $table
                ->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_groupings');
    }
}
