<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_categories', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this event category to a particular user
            $table
                ->integer('user')
                ->unsigned();

            $table
                ->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table
                ->string('name', 60);

            # See http://gamedev.stackexchange.com/questions/46463/how-can-i-find-an-optimum-set-of-colors-for-10-players
            $table
                ->string('color', 6);

            $table
                ->smallInteger('default_length')
                ->unsigned();

            $table
                ->smallInteger('default_capacity')
                ->unsigned();

            $table
                ->decimal('default_token_cost');

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_categories');
    }
}
