<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this calendar to a particular user
            $table
                ->integer('user')
                ->unsigned();

            $table
                ->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table
                ->string('name', 35);

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
