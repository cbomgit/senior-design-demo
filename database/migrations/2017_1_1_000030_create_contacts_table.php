<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {

            $table
                ->increments('id');

            # Tie this contact to a particular user
            $table
                ->integer('user')
                ->unsigned();
            $table
                ->foreign('user')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table
                ->string('first_name', 35);

            $table
                ->string('last_name', 35);

            $table
                ->string('phone', 11);

            $table
                ->string('email', 254);

            $table
                ->string('notes', 4000);

            $table
                ->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contacts');
    }
}