<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// 'middleware' => 'auth:api'
//Route::group(['prefix' => '1.0', 'middleware' => ['throttle:3000', 'auth:api']], function () {
Route::group(['prefix' => '1.0', 'middleware' => ['auth:api']], function () {

    Route::resource('user', 'APIUserController');

    Route::resource('event', 'APIEventController');

    Route::resource('eventcategory', 'APIEventCategoryController');

    Route::resource('eventgrouping', 'APIEventGroupingController');

    Route::resource('eventcalendarassoc', 'APIEventCalendarAssociationController');

    Route::resource('token', 'APITokenController');

    Route::resource('contactphone', 'APIContactPhoneController');

    Route::resource('contactemail', 'APIContactEmailController');

    Route::resource('contact', 'APIContactController');

    Route::resource('calendar', 'APICalendarController');

    Route::resource('attendance', 'APIAttendanceController');

});


