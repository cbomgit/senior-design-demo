<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Handle all authentication like a boss
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/dashboard', 'DashboardController@index');

Route::get('/event-form/{eventid?}', 'EventFormController@index');
Route::get('/view-event/{eventid}', 'EventDisplayController@index');


Route::get('/event-category/{eventCatId?}', 'EventCategoryFormController@index');
Route::get('/my-event-categories', 'EventCategoryListController@index');

Route::get('/calendar/{eventCatId?}', 'CalendarFormController@index');
Route::get('/my-calendars', 'CalendarListController@index');

Route::get('/contact-form/{contactid?}', 'ContactFormController@index');
Route::get('/my-contact-list', 'ContactListController@index');

Route::get('/contact-profile/{contactid}', 'ContactProfileController@index');
Route::get('/contact-email', 'ContactEmailController@index');

Route::get('/user-profile/{userid?}', 'UserController@index');

