@extends('layouts.single-col')

@section('stylesheets')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/css/eventform/eventform.css">

@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="/js/formSaver.js"></script>
    <script src="/js/eventform.js"></script>
    <script>

        var attendances = '';
        function saveForm() {

            if(formConfig.canSave()) {
                var event_title = $("#EventTitle").val();
                var start_datetime = (new Date($("#EventStart").val())).toISOString().split("T")[0] + " " + $("#EventFrom").val();
                var event_category = $("#category-select").val();
                var calendar = $("#calendar-select").val();
                var length_override = ($("#EventTo").val() * 60) + parseInt(($("#EventDurationMinutes").val()));

                formSaver.save({
                    'name'          :event_title,
                    'event_category':event_category,
                    'calendar':calendar,
                    'start_datetime':start_datetime,
                    'length_override':length_override
                });
            }
        }

        $(document).ready(function(){

            attendances = {!! isset($attendances) ? json_encode($attendances->toArray()) : json_encode(array()) !!} ;

            formConfig.init();
            //set up the form saver to update the correct resource if an id is passed
            formSaver.id = '{{ isset($id) ? '/'.$id : '' }}';

            //the type of resource the formSaver should update
            formSaver.type = 'event';

            //http method formSaver should use against the resource
            formSaver.method = '{{ isset($id) ? "PUT" : "POST" }}';

            //function to call when resource is updated or created
            formSaver.successCallback = successCallback;

        });
    </script>
@endsection

@section('content-left')
<div class="panel panel-default">
    <div class="panel-heading">Navigation</div>

    <div class="panel-body">

    </div>
</div>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <span class="h4">{{  isset($event) ? 'Edit ' . $event->name : 'Create a new event' }}</span>
        <span id="ajax-message" style="display: none;"></span>
        <span id="api_token" type="hidden" _token={{ $api_token }}></span>
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <p id="error-text"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                <div class="form-group">
                    <label for="EventTitle">Title</label>
                    <input type="text" class="form-control required" id="EventTitle" placeholder="Event Title (required)"
                        value="{{  isset($event) ? $event->name : '' }}">
                    <span class="required-text">This can't be blank</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12" id="attendee-list">
                <label>Attendees</label>
                <div id="contact-control">
                    <input type="text" id="contact-filter" class="form-control" placeholder="filter">
                    <ul id="contact-select">
                    @foreach($contacts as $contact)
                        <li class="contact-list-item available-attendee" data-contact-id="{{ $contact->id }}" >
                            <div class="contact-list-item-container row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <span class="contact-list-item-name">
                                        <a role="button">{{ $contact->first_name." ".$contact->last_name }}</a>
                                    </span>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <input type="number" min="0" class="form-control attendee-token-override">
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4">
                                    <img class='expand-form-icon' src="/images/expand-icon.png" width="24px" height="24px"/>
                                </div>
                            </div>
                            <div class="row attendance-form">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                    <select title='attendance status select' class="form-control attendee-status">
                                        <option value="RESERVED" selected>Scheduled</option>
                                        <option value="ATTENDED">Attended</option>
                                        <option value="ATTENDED_REFUNDED">Attended, Refunded</option>
                                        <option value="ATTENDED_PARTIAL_REFUND">Attended, Partial refund</option>
                                        <option value="CANCLED_REFUNDED">Canceled</option>
                                        <option value="CANCLED_NOT_REFUNDED">Canceled, no refund</option>
                                        <option value="CANCLED_PARTIAL_REFUND">Canceled, partial refund</option>
                                        <option value="RESCHEDULED_REFUNDED">Rescheduled, refunded</option>
                                        <option value="RESCHEDULED_NOT_REFUNDED">Reschedule, no refund</option>
                                        <option value="RESCHEDULED_PARTIAL_REFUND">Rescheduled, partial refund</option>
                                    </select>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                    <input class="form-control attendee-refund-percentage" type="number" min="0" placeholder="refund %" value="0.00">
                                </div>
                            </div>
                            <div class="row attendance-form">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <textarea class="form-control attendance-form-notes" placeholder="notes">No notes yet.</textarea>
                                    <a role="button" class="delete-attendance-link">Delete</a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                            <a role="button" onclick="formConfig.showAllContacts()">View all</a>
                        </div>
                        <div class="col-lg-6 col-md-4 col-sm-4 col-xs-4">
                            <a role="button" onclick="formConfig.viewAttending()">View attending</a>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                            <a href="/contact-form?returnurl=/event-form">Create</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="informational-text" class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                <p class="alert alert-warning">
                    Before creating an event, create at least 1
                    <a href="/calendar?returnurl=/event-form">calendar</a>
                    and
                    <a href="/event-category?returnurl=/event-form">event category.</a>
                </p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                <div class="form-group">
                    <label for="Calendar">Calendar</label>
                    <select title=Calendars" id="calendar-select" class="form-control required">
                @foreach($calendars as $calendar)
                    @if(isset($event) && ($calendar->id == $event->calendar))
                        <option class="calendar-list-item" value="{{ $calendar->id }}" selected>
                            {{ $calendar->name }}
                        </option>
                    @else
                        <option class="calendar-list-item" value="{{ $calendar->id }}">
                            {{ $calendar->name }}
                        </option>
                    @endif
                @endforeach
                    </select>
                    <span class="required-text">Please select a calendar, or create one
                        <a href="/calendar?returnurl=/event-form">here.</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                <div class="form-group">
                    <label for="EventCategories">Category</label>
                    <select title=EventCategories" id="category-select" class="form-control required">
                @foreach($categories as $category)
                    @if(isset($event) && ($category->id == $event->event_category))
                        <option class="event-category-list-item" value="{{ $category->id }}" selected>
                            {{ $category->name }}
                        </option>
                    @else
                    <option class="event-category-list-item" value="{{ $category->id }}">
                        {{ $category->name }}
                    </option>
                    @endif
                @endforeach
                    </select>
                    <span class="required-text">Please select a category, or create one
                        <a href="/event-category?returnurl=/event-form">here.</a>
                    </span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <div class="form-group">
                    <label for="EventStart">Start On</label>
                    @if(isset($event))
                    <input type="datetime" class="form-control required" id="EventStart" value="{{ explode(" ", $event->start_datetime)[0] }}">
                    @else
                    <input type="datetime" class="form-control required" id="EventStart" value="{{ getdate()['year'].'-'.getdate()['mon'].'-'.(getdate()['mday'] + 1)}}"/>
                    @endif
                    <span class="required-text">This can't be blank</span>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label for="EventFrom">Start Hour</label>
                    @if(isset($event))
                        <input type="time" class="form-control required" id="EventFrom" value="{{ explode(" ", $event->start_datetime)[1] }}">
                    @else
                        <input type="time" class="form-control required" id="EventFrom">
                    @endif
                    <span class="required-text">This can't be blank</span>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label for="AllDayEvent">All Day Event?</label>
                    <input type="checkbox" class="checkbox" id="AllDayEvent">
                </div>
            </div>
        </div>
        <div class="row" id="event-time-fields">
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>For</label>
                    @if(isset($event))
                    <input type="number" min='0' class="form-control required" id="EventTo" value="{{ (int) ($event->length_override / 60) }}">
                    @else
                    <input type="number" min='0' class="form-control required" id="EventTo" value="1">
                    @endif
                    <label for="EventTo">hours</label>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>and</label>
                    @if(isset($event))
                    <input type="number" min="0" max="60" class="form-control" value="{{ $event->length_override % 60 }}" id="EventDurationMinutes">
                    @else
                    <input type="number" min="0" max="60" class="form-control" value="0" id="EventDurationMinutes">
                    @endif
                    <label for="EventDurationMinutes">minutes</label>
                    <span class="required-text">Missing fields</span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-3 col-sm-3 col-xs-3">
                <div class="form-group">
                    <label for="recurrence-checkbox">Recurrence?</label>
                    <input type="checkbox" class="checkbox" id="recurrence-checkbox">
                </div>
            </div>
        </div>
        <div id="recurrence-form" class="row">
            <div class="col-lg-3 col-md-3" id="recurrence-type-radio">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="RecurrenceType" id="recurrence-type-daily" value="Daily">
                        Daily
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="RecurrenceType" id="recurrence-type-weekly" value="Weekly">
                        Weekly
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="RecurrenceType" id="recurrence-type-monthly" value="Monthly">
                        Monthly
                    </label>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="row" id="daily-recurrence-form">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-inline">
                            Repeat Every
                            <input min="0" type="number" class="form-control" id="repeat-every-x-days" name="RepeatEveryXDays" title="Repeat Every X Days" checked="checked">
                            Days(s)
                        </div>
                    </div>
                </div>
                <div class="row" id="weekly-recurrence-form">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-inline">
                            Repeat Every
                            <input type="number" min="0" class="form-control" id="repeat-every-x-weeks" name="RepeatEveryXWeeks" title="Repeat Every X Weeks">
                            week(s) on:
                        </div>
                        <div class="form-group">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-sun" value="Sunday">Sun
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-mon" value="Monday">Mon
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-tues" value="Tuesday">Tues
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-wed" value="Wednesday">Wed
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-thurs" value="Thursday">Thurs
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-fri" value="Friday">Fri
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" id="repeat-on-sat" value="Saturday">Sat
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="monthly-recurrence-form">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-inline">
                            Day <input min="0" max="31" class="form-control" type="number" id="repeat-on-x-day" title="Repeat on Day">
                            of every <input min="0" class="form-control" type="number" id="repeat-every-x-months" title="Repeat Every X Months">
                            month(s)
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="recurrence-length-form">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="recurrent-repeat-length">
                        <div class="form-check-inline form-inline">
                            <label class="form-check-label form-inline">
                                <input class="form-check-input" type="radio" name="RecurrentRepeatType" id="recurrence-until-Date" value="RecurUntilDate">
                                Recur until <input type="datetime" id="RecurUntilDatePicker" class="form-control"/>
                            </label>
                        </div>
                        <div class="form-check-inline form-inline">
                            <label class="form-check-label form-inline">
                                <input class="form-check-input" type="radio" name="RecurrentRepeatType" id="recurrence-n-times" value="RepeatNTimes">
                                Repeat this recurrence <input type="number" min="0" id="RepeatNTimesInput" class="form-control"/> times.
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                <div class="form-group">
                    <input class="btn btn-primary" value="Save" type="submit" onclick="saveForm()">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
                <div class="form-group">
                    <input class="btn btn-primary" value="Cancel" type="submit" onclick="formSaver.navToReturnUrl()">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="display:{{ isset($id) ? 'inherit' : 'none' }}">
                <div class="form-group">
                    <input class="btn btn-primary" value="Delete" type="submit" onclick="formSaver.delete()">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
