@extends('layouts.single-col')

@section('stylesheets')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!--<link rel="stylesheet" href="/css/contactform/contactform.css"> -->
    <script type="text/javascript"></script>
@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
@endsection

@section('content-left')
    <div class="panel panel-default">
        <div class="panel-heading">Navigation</div>

        <div class="panel-body">

        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 style="display:inline-block">Manage my credentials</h4>
            <!--<a href="/contact-form?returnUrl=/my-contact-list" style="float:right; margin-top: 12px">Create New Contact</a>-->
        </div>

        <div class="panel-body">
            <span id="ajax-message" style="display: none;"></span>
            <span id="api_token" type="hidden" _token={{ $api_token }}></span>
            <table id="contact-table" class="table table-responsive">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>E-Mail</th>
                    <th>Password</th>
                </tr>
                @foreach($users as $user)
                    <tr>
                        <td>
                            {{ $user->first_name }}
                        </td>
                        <td>
                            {{ $user->last_name }}
                        </td>
                        <td>
                            {{ $user->email }}
                        </td>
                        <td>
                            {{ $user->password }}
                        </td>
                        <!--<td>
                            <a href="/contact-profile/{{ $contact->id }}?returnUrl=/my-contact-list">Profile</a>|
                            <a href="/contact-form/{{ $contact->id }}?returnUrl=/my-contact-list">Edit </a>|
                            <a href="" onclick="deleteContact({{ $contact->id }})"> Delete</a>
                        </td>-->
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
