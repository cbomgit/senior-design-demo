@extends('layouts.single-col')

@section('stylesheets')
    <style type="text/css">

        .container-right {
            width: 100%;
        }

        #error-text {
            color:red;
        }

        #ajax-message {
            color: limegreen;
        }

        .sp-preview {
            width: 80% !important;
        }

        .required-text {
            font-size: .8em;
            color: red;
            font-style: italic;
            display:none;
        }
    </style>
    <link rel="stylesheet" href="/css/colorpicker/spectrum.css">
@endsection

@section('scripts')
    <script src="/js/lib/colorpicker/spectrum.js"></script>
    <script src="/js/lib/APIResource.js"></script>
    <script src="/js/eventCategory.js"></script>
    <script>

        function getReturnUrl() {
            var paramMatches = window.location.href.match(/returnurl=\/\S+(\/\d+)*/);
            var returnUrl = '/dashboard';

            if(paramMatches && paramMatches.length > 0) {
                returnUrl = paramMatches[0].split("=").length == 2 ? paramMatches[0].split("=")[1] : '/dashboard';
            }
            else {
                returnUrl = '/dashboard';
            }

            return returnUrl;
        }

        //general function to indication a failed AJAX call
        function ajaxFailure(error) {

            var message = "Status code: " + error.status + ", Message: " + error['responseJSON'].error.message;

            $("#error-text").html(message)
                .parents('.alert')
                .show(200);
        }

        function deleteCategory() {

            var categoryId = '{{ isset($id) ? $id : '' }}';
            var token = $("span#api_token").attr("_token");
            var categoryAPI = new API('eventcategory', token);
            $("#ajax-message").html("Saving...").show();

            categoryAPI.delete(categoryId).then(function(result) {

                $("#ajax-message").html("Done!");
                window.open(getReturnUrl(), '_self');

            }, ajaxFailure);
        }

        function saveForm() {

            debugger;

            if(validate()) {

                var name = document.getElementById('name').value;
                var color = $("#color").spectrum("get").toString().replace("#", "");
                var default_length = document.getElementById('default_length').value;
                var default_capacity = document.getElementById('default_capacity').value;
                var default_token_cost = document.getElementById('default_token_cost').value;

                var body = {
                    "name":fieldsLookup['name'].getValue(),
                    "color": fieldsLookup['color'].getValue(),
                    "default_length" : fieldsLookup['default_length'].getValue(),
                    "default_capacity" : fieldsLookup['default_capacity'].getValue(),
                    "default_token_cost" : fieldsLookup['default_token_cost'].getValue()
                };

                var categoryId = '{{ isset($id) ? $id : '' }}';
                var token = $("span#api_token").attr("_token");
                var categoryAPI = new API('eventcategory', token);

                $("#ajax-message").html("Saving...").show();
                if(categoryId == '') {
                    categoryAPI.post(body).then(function(result) {
                        $("#ajax-message").html("Done!");
                        window.open(getReturnUrl(), '_self');
                    }, ajaxFailure);
                }
                else {

                    categoryAPI.put(categoryId, body).then(function(result) {
                        $("#ajax-message").html("Done!");
                        window.open(getReturnUrl(), '_self');
                    }, ajaxFailure);

                }
            }

        }
    </script>
@endsection

@section('content-left')
<div class="panel panel-default">
    <div class="panel-heading">Navigation</div>

    <div class="panel-body">

    </div>
</div>
@endsection

@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
        <span class="h4">{{  isset($eventCategory) ? 'Editing Event Category: ' . $eventCategory->name : 'Create a new event category' }}</span>
        <span id="ajax-message" style="display: none;"></span>
        <span id="api_token" type="hidden" _token={{ $api_token }}></span>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                <p id="error-text"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">
                <div class="form-group">
                    <label for="name">Category Name</label>
                    <input class="form-control" name="name" id="name" type="text" value="{{ isset($eventCategory) ? $eventCategory->name : '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-8 col-xs-8">
                <div class="form-group">
                    <label for="color">Color</label>
                    <input class="form-control" name="color" id="color" type="color" value="{{ isset($eventCategory) ? "#".$eventCategory->color : '#345345' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-8 col-xs-8">
                <div class="form-group">
                    <label for="default_length">Default Duration (in minutes)</label>
                    <input class="form-control" min="0" name="default_length" id="default_length" type="number" value="{{ isset($eventCategory) ? $eventCategory->default_length : '60' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-8 col-xs-8">
                <div class="form-group">
                    <label for="default_capacity">Default Capacity</label>
                    <input class="form-control" min="0" name="default_capacity" id="default_capacity" type="number" value="{{ isset($eventCategory) ? $eventCategory->default_capacity : '1' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-8 col-xs-8">
                <div class="form-group">
                    <label for="default_token_cost">Default Token Cost</label>
                    <input class="form-control" min="0" name="default_token_cost" id="default_token_cost" type="number" value="{{ isset($eventCategory) ? $eventCategory->default_token_cost : '1' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
                    <input class="btn btn-primary" value="Save" type="submit" onclick="saveForm()">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
                    <input class="btn btn-primary" value="Cancel" type="submit" onclick="window.open(getReturnUrl(), '_self')">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" style="display:{{ isset($id) ? 'inherit' : 'none' }}">
                <div class="form-group">
                    <input class="btn btn-primary" value="Delete" type="submit" onclick="deleteCategory()">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
