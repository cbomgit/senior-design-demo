<!DOCTYPE html>
<html>
    <head>
        <title>404 Not Found</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            h1, p {
                color: grey;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div>
                    <h1>404 Page Not Found</h1>
                    <p> Sorry, the page you are looking for does not exist</p>
                </div>
            </div>
        </div>
    </body>
</html>
