
@extends('layouts.single-col')

@section('stylesheets')
    <style type="text/css">

        tr:first-child th {
            border-top: none !important;
        }

        .event-edit-link {
            padding-left:10px;
        }

        #accordion-complete {
            height: 400px;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript"></script>

@endsection

@section('scripts')

@endsection


@section('content')
    <div class="panel panel-default container">
        <div class="panel-heading">
            <h4 style="display:inline-block">Contact Profile</h4>
            <a href="/contact-form/?returnurl=/my-contact-list" style="float:right; margin-top: 12px">Create New Contact</a>
        </div>

        <div class="row">
            <div class="col-xs-8">
                <span id="ajax-message" style="display: none;"></span>
                <span id="api_token" type="hidden" _token={{ $api_token }}></span>
                <h3>
                    {{ $contact->first_name }} {{$contact->last_name}}
                    <a href="/contact-form/{{ $contact->id }}?returnUrl=/contact-profile/{{ $contact->id }}">
                        <img src="/images/edit-icon.png" height="16px" width="auto" class="event-edit-link"/>
                    </a>
                </h3>
                <h4>Email:
                    <a href="mailto:{{ $contact->email }}">{{  $contact->email }}</a>
                </h4>
                <h4>Phone: {{ $contact->phone }}</h4>
            </div>
            <div class="col-xs-4">
                <p class="h5 text-right">token balance: </p>
                <p class="h1 text-right">{{ $balance }}</p>
            </div>
            <div class="col-xs-12">
                <h4>Pending Appointments</h4>
                <div class="panel-group" id="accordion-pending">
                    @foreach($pending as $item)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion-pending" href="#pending-collapse{{ $loop->index }}">
                                    {{ $item['event']->name }}
                                </a>
                                <span>{{ date('Y-m-d h:i a', strtotime($item['event']->start_datetime)) }}</span>
                                <a href="/event-form/{{ $item['event']->id }}?returnUrl=/contact-profile/{{ $contact->id }}">
                                    <img src="/images/edit-icon.png" height="16px" width="auto" class="event-edit-link"/>
                                </a>
                            </h4>

                        </div>
                        <div id="pending-collapse{{ $loop->index }}" class="panel-collapse collapse out">
                            <div class="panel-body">
                                Notes: {{ $item['attendance']->status }}<br>
                                {{ $item['attendance']->notes }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="col-xs-12">
                <h4>Past Events</h4>
                <div class="panel-group" id="accordion-complete">
                    @foreach($complete as $item)
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-complete" href="#complete-collapse{{ $loop->index }}">
                                        {{ $item['event']->name }}
                                    </a>
                                    <span>{{ date('Y-m-d h:i a', strtotime($item['event']->start_datetime)) }}</span>
                                    <a href="/event-form/{{ $item['event']->id }}?returnUrl=/contact-profile/{{ $contact->id }}">
                                        <img src="/images/edit-icon.png" height="16px" width="auto" class="event-edit-link"/>
                                    </a>
                                </h4>
                            </div>
                            <div id="complete-collapse{{ $loop->index }}" class="panel-collapse collapse out">
                                <div class="panel-body">
                                    {{ date('Y-m-d h:i a', strtotime($item['event']->start_datetime)) }}
                                    Amount: {{ $item['token']->pending == 0 ? $item['token']->amount.' token(s)' : 'n/a' }}
                                    <br>
                                    Notes: {{ $item['attendance']->status }}<br>
                                    {{ $item['attendance']->notes }}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
