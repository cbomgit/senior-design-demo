@extends('layouts.single-col')

@section('stylesheets')
    <style type="text/css">

        tr:first-child th {
            border-top: none !important;
        }

    </style>
    <script type="text/javascript"></script>

@endsection

@section('scripts')
    <script src="/js/formSaver.js"></script>
    <script>
        function deleteContact(id) {
            formSaver.id = '/'+id;
            formSaver.type = 'contact';
            formSaver.method = 'DELETE';
            formSaver.returnurl = window.location.href;
            formSaver.delete();
            debugger;
        }
    </script>
@endsection

@section('content-left')
    <div class="panel panel-default">
        <div class="panel-heading">Navigation</div>

        <div class="panel-body">

        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 style="display:inline-block">My Contacts</h4>
            <a href="/contact-form?returnUrl=/my-contact-list" style="float:right; margin-top: 12px">Create New Contact</a>
        </div>

        <div class="panel-body">
            <span id="ajax-message" style="display: none;"></span>
            <span id="api_token" type="hidden" _token={{ $api_token }}></span>
            <table id="contact-table" class="table table-responsive">
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone Number</th>
                    <th>E-Mail</th>
                    <th>Note</th>
                </tr>
                @foreach($contacts as $contact)
                    <tr>
                        <td>
                            {{ $contact->first_name }}
                        </td>
                        <td>
                            {{ $contact->last_name }}
                        </td>
                        <td>
                            {{ $contact->phone }}
                        </td>
                        <td>
                            {{ $contact->email }}
                        </td>
                        <td>
                            {{ $contact->notes }}
                        </td>
                        <td>
                            <a href="/contact-profile/{{ $contact->id }}?returnUrl=/my-contact-list">Profile</a>|
                            <a href="/contact-form/{{ $contact->id }}?returnUrl=/my-contact-list">Edit </a>|
                            <a href="" onclick="deleteContact({{ $contact->id }})"> Delete</a>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
