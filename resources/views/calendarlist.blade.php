@extends('layouts.single-col')

@section('stylesheets')
    <style type="text/css">
        .container-right {
            width: 100%;
        }

        span.color-cell {
            display: inline-block;
            background-color: red;
            height: 20px;
            width: 40px;
            border: 1px solid grey;
            border-radius: 4px;
        }

        tr:first-child th {
            border-top: none !important;
        }
    </style>

@endsection

@section('scripts')
<script src="/js/formSaver.js"></script>
    <script>
        function deleteCalendar(id) {
            formSaver.id = '/'+id;
            formSaver.type = 'calendar';
            formSaver.method = 'DELETE';
            formSaver.returnurl = window.location.href;
            formSaver.delete();
        }
    </script>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 style="display:inline-block">My Calendars</h4>
        <a href="/calendar?returnurl=/my-calendars" style="float:right; margin-top: 12px">Create New Calendar</a>
    </div>

    <div class="panel-body">
        <span id="ajax-message" style="display: none;"></span>
        <span id="api_token" type="hidden" _token={{ $api_token }}></span>
        <table id="category-table" class="table table-responsive">
            <tr>
                <th>Name</th>
                <th></th>
            </tr>
            @foreach($calendars as $calendar)
            <tr>
                <td>
                    {{ $calendar->name }}
                </td>
                <td align="right">
                    <a href="/calendar/{{ $calendar->id }}?returnurl=/my-calendars">Edit</a> |
                    <a href="" onclick="deleteCalendar({{ $calendar->id }})">Delete</a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
