@extends('layouts.single-col')

@section('stylesheets')
    <style type="text/css">
        .container-right {
            width: 100%;
        }

        span.color-cell {
            display: inline-block;
            background-color: red;
            height: 20px;
            width: 40px;
            border: 1px solid grey;
            border-radius: 4px;
        }

        tr:first-child th {
            border-top: none !important;
        }

        /* responsive table styles */
        th:nth-last-child(2), th:nth-last-child(4),
        td:nth-last-child(2), td:nth-last-child(4) {
            display:none;
        }

        @media (min-width: 768px) {
            th:nth-last-child(2), td:nth-last-child(2) {
                display:none;
            }

            th:nth-last-child(4), td:nth-last-child(4) {
                display:block;
            }

        }

        @media(min-width:992px) {
            /* responsive table styles */
            th:nth-last-child(2), th:nth-last-child(4),
            td:nth-last-child(2), td:nth-last-child(4) {
                display:block;
            }
        }
    </style>

@endsection

@section('scripts')
<script src="/js/lib/APIResource.js"></script>
    <script>

        //general function to indication a failed AJAX call
        function ajaxFailure(error) {
            var message = "Status code: " + error.status + ", Message: " + error['responseJSON'].error.message;
            $("#error-text").html(message)
                .parents('.alert')
                .show(200);
        }

        function deleteCategory(id) {
            var token = $("#api_token").attr("_token");
            var categoryAPI = new API('eventcategory', token);
            $("#ajax-message").html("Deleting...").show();
            categoryAPI.delete(id).then(function(result) {
                $("#ajax-message").html("Done!");
                window.open(window.location.href + "?q=1", "_self")
            }, ajaxFailure);
        }
    </script>
@endsection

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">
        <h4 style="display:inline-block">My Event Categories</h4>
        <a href="/event-category?returnurl=/my-event-categories" style="float:right; margin-top: 12px">Create New Event Category</a>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                <p id="error-text"></p>
            </div>
        </div>
        <span id="ajax-message" style="display: none;"></span>
        <span id="api_token" type="hidden" _token={{ $api_token }}></span>
        <table id="category-table" class="table table-responsive">
            <tr>
                <th>Name</th>
                <th>Color</th>
                <th>Default Capacity</th>
                <th>Default Duration</th>
                <th>Default Token Cost</th>
                <th></th>
            </tr>
            @foreach($categories as $category)
            <tr>
                <td>
                    {{ $category->name }}
                </td>
                <td>
                    <span class="color-cell" style="background-color: #{{ $category->color }}"></span>
                </td>
                <td>
                    {{ $category->default_capacity }}
                </td>
                <td>
                    {{ $category->default_length }}
                </td>
                <td>
                    {{ $category->default_token_cost }}
                </td>
                <td>
                    <a href="/event-category/{{ $category->id }}?returnurl=/my-event-categories">
                        <img src="/images/edit-icon.png" alt="Edit" height="16px" width="auto"/>
                    </a>
                    <a href="" onclick="deleteCategory({{ $category->id }})">
                        <img src="/images/trash-icon.png" alt="Delete" height="16px" width="auto"/>
                    </a>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
