@extends('layouts.single-col')

@section('stylesheets')
    <!--<link rel="stylesheet" href="/css/contactform/contactform.css"> -->
    <script type="text/javascript"></script>

    <style type="text/css">
        span.required-text {
            font-size: .8em;
            color: red;
            font-style: italic;
            display: none;
        }

    </style>
@endsection

@section('scripts')

    <script src="/js/formSaver.js"></script>
    <script src="/js/contactForm.js"></script>
    <script>


        function cancel() {
            var paramMatches = window.location.href.match(/returnUrl=\/\S+\/\d+/);
            if(paramMatches.length > 0) {
                var returnUrl = paramMatches[0].split("=").length == 2 ? paramMatches[0].split("=")[1] : '/dashboard';
                window.open(returnUrl, '_self');
            }
            else {
                window.open('/dashboard', '_self');
            }
        }
        function saveForm() {

            if(contactFunct.okToSave()){
                debugger;
                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var email = $("#email").val();
                var phone = $("#phone").val();
                var notes = $("#notes").val();

                if (validateEmail(email)) {
                    //alert('Nice!! your Email is valid, now you can continue..');
                }
                else {
                    alert('Invalid Email Address');
                    e.preventDefault();
                }

                if (validatePhone(phone)) {
                    //alert('Nice!! your Email is valid, now you can continue..');
                }
                else {
                    alert('Invalid Phone Number Format');
                    e.preventDefault();
                }

                // Function that validates email address through a regular expression.
                function validateEmail(email) {
                    var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
                    if (filter.test(email)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

                //Function that validates phone number through a regular expression.
                function validatePhone(phone) {
                    var filter = /^(\d{3})(\d{3})(\d{4})$/;
                    if (filter.test(phone)) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }


                formSaver.save({
                    "first_name": first_name,
                    "last_name": last_name,
                    "email": email,
                    "phone": phone,
                    "notes": notes
                });

            }
            //console.log(result);
        }

        $(document).ready(function(e) {

            formSaver.id = '{{ isset($id) ? '/'.$id : '' }}';
            formSaver.type = 'contact';
            formSaver.method = '{{ isset($id) ? "PUT" : "POST" }}';

        });


    </script>
@endsection

@section('content-left')
    <div class="panel panel-default">
        <div class="panel-heading">Navigation</div>

        <div class="panel-body">

        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="h4">{{  isset($contact) ? 'Editing Contact: ' . $contact->id : 'Add a new contact' }}</span>
            <span id="ajax-message" style="display: none;"></span>
        <span id="api_token" type="hidden" _token={{ $api_token }}></span>

        <div class="panel-body">
            <div class="col-xs-12">
                <div style="display:none" class="alert alert-danger col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p id="error-text"></p>
                </div>
            </div>
            <div class="form-group">
                <label> First Name: (required)*
                    <input placeholder="Please enter your name" id="first_name" type="text" size="40" tabindex="1"
                           required autofocus class="form-control required" value="{{ isset($contact) ? $contact->first_name : '' }}">
                    <span class="required-text">please type a first name</span>
                </label>
            </div>
            <div class="form-group">
                <label> Last Name: (required)*
                    <input placeholder="Please enter your Last name" id="last_name" type="text" size="40" tabindex="2"
                           required autofocus class="form-control required" value="{{ isset($contact) ? $contact->last_name : '' }}">
                    <span class="required-text">please type a last name</span>
                </label>
            </div>

            <div class="form-group">
                <label> Email:
                    <input placeholder="name@email.com" id="email" type="email" size="40" tabindex="3"
                           required autofocus class="form-control required" value="{{ isset($contact) ? $contact->email:'' }}">
                    <span class="required-text">please type a valid email</span>
                </label>
            </div>
            <div class="form-group">
                <label>Telephone: (required)*
                   <input placeholder="5554443322" id="phone" type="tel" size="40" tabindex="4"
                          required autofocus class="form-control required" value="{{ isset($contact) ? $contact->phone : '' }}">
                    <span class="required-text">please type a phone number </span>
                </label>
            </div>
            <div class="form-group">
                <label> <span>Note: </span>
                    <textarea placeholder="Include details you'd like to remember" id="notes" size="40" tabindex="6"
                              required class="form-control">{{ isset($contact) ? $contact->notes : '' }}</textarea>
                </label>
            </div>
            <div class="form-group">
                <input class="btn btn-primary" value="Save" type="submit" onclick="saveForm()">
                <a class="btn btn-primary" value="Cancel" type="submit" href="/contact-form"> Reset Form </a>
                <a class="btn btn-primary" value="Cancel" type="submit" onclick="cancel()"> Cancel </a>
            </div>
        </div>
    </div>
@endsection
