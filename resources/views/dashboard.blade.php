@extends('layouts.two-col')

@section('stylesheets')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link href='/css/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
    <link href='/css/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>

    <style type="text/css">

        span.color-cell {
            display: inline-block;
            background-color: red;
            height: 22px;
            width: 40px;
            border: 0;
            border-radius: 5px;
            float: right;
        }

        .toggleItem {
            clear: both;
            overflow: auto;
            margin: 0 0 11px 0;
            padding: 0 0 0 5px;
            vertical-align: middle;
        }

        .activated {
            border-radius: 5px;
            background: #f2f2f2;
        }

    </style>

@endsection

@section('scripts')
    <script src='/js/lib/moment.min.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src='/js/fullcalendar/fullcalendar.min.js'></script>
    <script src="/js/lib/APIResource.js"></script>
    <script>

        var token = '{{ $api_token }}';
        var disableUpdate = false;
        var change = null;

        $( ".toggleItem" ).click(function() {

            $(this).toggleClass('activated');

            $("#calendar").fullCalendar('refetchEvents');

        });

        function updateRecurrence(changeObj) {

            debugger;
            var date_diff = changeObj.date_diff.asMinutes();
            var api = new API('eventgrouping', token);
            api.put(changeObj.groupId, { 'date_diff': date_diff})
                .then(function(result) {
                    $("#recurrence-modal").modal("hide");
                    disableUpdate = false;
                })
        }

        function updateRecurrenceLength(changeObj) {

            var length_override = moment.duration(changeObj.event.end.diff(changeObj.event.start)).asMinutes();

            var api = new API('eventgrouping', token);
            api.put(changeObj.groupId, { 'length_override': length_override})
                .then(function(result) {
                    $("#recurrence-time-modal").modal("hide");
                    disableUpdate = false;
                });

        }

        function updateEventDate(event){

            var api = new API('event', token);

            var requestBody = {
                'start_datetime': event.start.format("YYYY-MM-DD HH:mm:ss"),
                'event_group':'null'
            };

            api.put(event.id, requestBody).then(function(result) {

                $("#recurrence-modal").modal('hide');

                disableUpdate = false;
            });

        }

        function updateEventTime(event) {

            var ms = moment(event.end,"DD/MM/YYYY HH:mm:ss").diff(moment(event.start,"DD/MM/YYYY HH:mm:ss"));
            var duration = moment.duration(ms).asMinutes();
            var api = new API('event', token);

            var body = {
                'length_override': duration,
                'event_group': 'null'
            };

            api.put(event.id, body).then(function(response) {
                disableUpdate = false;
                $("#recurrence-time-modal").modal("hide");
            });
        }


        function fetchEvents(start, end, timezone, callback) {

            var fetchedEvents = [];

            var cals = 'null';

            $('.calendar.activated').each(function(i) {
                var id = $(this).data('id');
                if(i == 0)
                    cals = String(id);
                else
                    cals = cals + ',' + id;
            });

            var cats = 'null';

            $('.category.activated').each(function(i) {
                var id = $(this).data('id');
                if(i == 0)
                    cats = String(id);
                else
                    cats = cats + ',' + id;
            });
            //console.log('Cals: ' + cals + ' Cats: ' + cats);

            var start = $('#calendar').fullCalendar('getView').start.format();
            var end = $('#calendar').fullCalendar('getView').end.format();

            var api = new API('event', token);
            var queryString = 'start=' + start + '&end=' + end + '&cal=' + cals + '&cat=' + cats;
            api.getAll(queryString).then(function(response) {

                for(var i = 0; i < response.payload.length; i++) {

                    var allDay = response.payload[i].length_override % parseInt('1440') == 0;
                    var duration = response.payload[i].length_override;
                    var start_date = moment(response.payload[i].start_datetime);
                    var end_date = moment(start_date).add(duration, 'm');
                    fetchedEvents.push({
                        id: response.payload[i].id,
                        'title': response.payload[i].name,
                        'start': start_date,
                        'end'  : end_date,
                        'color': '#' + response.payload[i].event_category_color,
                        'url': '/event-form/'+response.payload[i].id+'?returnurl=/dashboard',
                        'allDay' : allDay,
                        'recurrenceId': response.payload[i].event_group
                    });

                }

                callback(fetchedEvents);

            }, function(error) {

                console.log(error.responseJSON);

            });
        }

        $(document).ready(function () {

            $('#calendar').fullCalendar({

                defaultDate: (new Date()),
                editable: true,
                eventLimit: true,
                navLinks: true,
                nowIndicator: true,
                customButtons: {
                    newEvent: {
                        text: 'Create New',
                        click: function () {
                            var returnView = $("#calendar").fullCalendar("getView").name;
                            window.location = '/event-form?returnurl=/dashboard&returnView='+returnView;
                        }
                    },
                },
                header: {
                    left: 'prev,next today,newEvent',
                    center: 'title',
                    right: 'month,basicWeek,listWeek,agendaDay'
                },
                events: fetchEvents,
                eventDragStart: function(event, jsEvent, ui, view ) {
                    disableUpdate = true;
                   // console.log("Drag started...");

                },
                eventDragStop: function( event, jsEvent, ui, view ) {
                    disableUpdate = false;
                    //console.log("Drag stopped.");
                },
                eventDrop: function(event, delta, revertFunc) {


                    if(event.recurrenceId == null || event.recurrenceId == '0') {
                        updateEventDate(event);
                    }
                    else {
                        change = {
                            date_diff: delta,
                            groupId : event.recurrenceId,
                            event: event
                        };
                        $("#recurrence-modal").modal('show');
                    }


                },
                eventResizeStart: function(event, jsEvent, ui, view) {
                    disableUpdate = true;
                },
                eventResizeStop: function(event, jsEvent, ui, view) {
                    disableUpdate = false;
                },
                eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {

                    if(event.recurrenceId == null || event.recurrenceId == '0') {
                        updateEventTime(event);
                    }
                    else {
                        changedEvent = {event: event, delta: delta, groupId : event.recurrenceId};
                        $("#recurrence-time-modal").modal('show');
                    }

                },
                eventRender: function(event, element) {
                    var contentStr = event.allDay ? "All Day" : event.start.format("h:mm a") + " - "  + event.end.format("h:mm a");

                    element.popover({
                        'title':event.title,
                        'content': contentStr,
                        'animated':'true',
                        'container':'#calendar',
                        'placement':'right',
                        'delay': {
                            show:5000,
                            hide:500
                        },
                        'trigger':'hover'
                    });
                },
                eventMouseover: function( event, jsEvent, view ) {

                    $(this).popover("show");

                },
                eventMouseout: function( event, jsEvent, view) {

                    $(this).popover("hide");

                },
                eventClick: function(event) {

                    var returnView = $("#calendar").fullCalendar("getView").name;
                    var url = event.url+'&returnView='+returnView;
                    window.open(url, '_self');

                },
                dayClick: function(date, jsEvent, view) {
                    var returnView = $("#calendar").fullCalendar("getView").name;
                    window.open('/event-form?returnurl=/dashboard&startdate='+date.format()+'&returnView='+returnView, '_self');
                }
            });

            setInterval(function () {

                // Do not update events when the disabled flag is raised
                if(!disableUpdate) {
                    $("#calendar").fullCalendar('refetchEvents');
                    //console.log('Events fetched');
                }
            }, 3000);


            /*$("#hiddenDate").datepicker({
                dateFormat: "yy-mm-dd",
                currentText: "Now",
                showButtonPanel: true,
                appendText: "(yyyy-mm-dd)",
                constrainInput: true,
                changeMonth: true,
                changeYear: true,
                onClose: function(dateText, inst) {
                    if(dateText != "") {
                        $("#calendar").fullCalendar("gotoDate", dateText);
                    }
                }
            });

            $(".fc-goToDate-button").first().after($("#ui-datepicker-div"))*/
        });

    </script>
@endsection

@section('content-left')



    <div class="panel panel-default">

        <div class="panel-heading">Calendars</div>

        <div class="panel-body">

            @unless (count($calendars))
                <p>You don't have any event calendars yet, why not <a href="{{ action('CalendarFormController@index') }}?returnurl=/dashboard">create one</a>?</p>
            @endunless

            @foreach ($calendars as $calendar)
                <div data-id="{{ $calendar->id }}" class="calendar toggleItem activated">
                    <p style="margin: 0;">{{ $calendar->name }}</p>
                </div>
            @endforeach

        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading">Event Categories</div>

        <div class="panel-body">

            @unless (count($categories))
                <p>You don't have any event categories yet, why not <a href="{{ action('EventCategoryFormController@index') }}?returnurl=/dashboard">create one</a>?</p>
            @endunless

            @foreach ($categories as $category)
                <div data-id="{{ $category->id }}" class="category toggleItem activated">
                    <p style="float: left; margin: 0;">{{ $category->name }}</p>
                    <span class="color-cell" style="background-color: #{{ $category->color }}"></span>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('content-right')
    <span id="api_token" type="hidden" _token={{ $api_token }}></span>
    <div class="panel panel-default">
        <div class="panel-heading">Dashboard</div>

        <div class="panel-body">
            <div id='calendar'></div>

            <!-- modals for recurrence operations -->
            <div id="recurrence-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modify event or recurrence</h4>
                        </div>
                        <div class="modal-body">
                            <p>This event is part of a recurrence. Do you want to modify just this event or would you like
                                to delete the entire recurrence? If you choose to modify just this event, then it will
                                no longer be part of its event group.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" onclick="updateRecurrence(change)">Entire Series</button>
                            <button type="button" class="btn btn-default" onclick="updateEventDate(change.event)">Just this one</button>
                        </div>
                    </div>

                </div>
            </div>
            <div id="recurrence-time-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Modify event or recurrence</h4>
                        </div>
                        <div class="modal-body">
                            <p>This event is part of a recurrence. Do you want to modify just this event or would you like
                                to delete the entire recurrence? If you choose to modify just this event, then it will
                                no longer be part of its event group.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" onclick="updateRecurrenceLength(changedEvent)">Entire Series</button>
                            <button type="button" class="btn btn-default" onclick="updateEventTime(changedEvent.event)">Just this one</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection
