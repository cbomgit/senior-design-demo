@extends('layouts.single-col')

@section('stylesheets')
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="/js/lib/timepicker/jquery.timepicker.css">
    <link rel="stylesheet" href="/css/eventform/eventform.css">

@endsection

@section('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="/js/lib/moment.min.js"></script>
    <script src="/js/lib/timepicker/jquery.timepicker.js"></script>
    <script src="/js/lib/APIResource.js"></script>
    <script src="/js/eventFormFields.js"></script>
    <script src="/js/eventform.js"></script>

    <script>

        var attendances = '';
        var eventId = '{{ isset($id) ? $id : false }}';

        function createEvent() {

            if(canSave()) {

                if($("#recurrence-type").val() == 'recur-none') {
                    uploadFormData(eventId);
                }
                else {
                    createRecurrence(eventId);
                }
            }

        }

        function editOrCreate(event) {

            event.preventDefault();
            //if creating a new event call createEvent()
            if(!eventId) {
                createEvent();
            }
            else {

                //if the event is not part of a recurrence just edit the event
                var groupId = '{{ isset($event) ? $event->event_group : false }}';

                if(!groupId || groupId == null || groupId == '0') {
                    uploadFormData(eventId);
                }
                else {
                    $("#edit-recurrence-modal").modal("show");
                }
            }

        }

        function setTitle(title) {
            $("#EventTitle").val(title);
        }

        $(document).ready(function(){

            attendances = {!! isset($attendances) ? json_encode($attendances->toArray()) : json_encode(array()) !!} ;

            formConfig.init();


        });
    </script>
@endsection

@section('content-left')
    <div class="panel panel-default">
        <div class="panel-heading">Navigation</div>

        <div class="panel-body">

        </div>
    </div>
@endsection

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <span class="h4">{{  isset($event) ? 'Edit ' . $event->name : 'Create a new event' }}</span>
            <span id="ajax-message" style="display: none;"></span>
            <span id="api_token" type="hidden" _token={{ $api_token }}></span>


        </div>
        <div class="panel-body">
            <div class="col-xs-12">
                <div style="display:none" class="alert alert-danger col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <p id="error-text"></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="EventTitle">Title</label>
                            <input type="text" class="form-control required" id="EventTitle" placeholder="Event Title (required)"
                                   value="{{  isset($event) ? $event->name : '' }}">
                        </div>
                    </div>
                    <div id="informational-text" class="col-xs-12">
                        <p class="alert alert-warning">
                            Before creating an event, create at least 1
                            <a href="/calendar?returnurl=/event-form">calendar</a>
                            and
                            <a href="/event-category?returnurl=/event-form">event category.</a>
                        </p>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="Calendar">Calendar</label>
                            <select title=Calendars" id="calendar-select" class="form-control required">
                                @foreach($calendars as $calendar)
                                    @if(isset($event) && ($calendar->id == $event->calendar))
                                        <option class="calendar-list-item" value="{{ $calendar->id }}" selected>
                                            {{ $calendar->name }}
                                        </option>
                                    @else
                                        <option class="calendar-list-item" value="{{ $calendar->id }}">
                                            {{ $calendar->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                            <a role="button" onclick="showNewCalendarForm()">Create one</a>
                            <div id="new-calendar-group" class="form-inline" style="display:none;" >
                                <label for="new-calendar">Name</label>
                                <input title="new-calendar" class="form-control" id="new-calendar" type="text">
                                <a role="button" onclick="postNewCalendar()">Done</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="EventCategories">Category</label>
                            <select title=EventCategories" id="category-select" class="form-control required">
                                @foreach($categories as $category)
                                    @if(isset($event) && ($category->id == $event->event_category))
                                        <option class="event-category-list-item" value="{{ $category->id }}" selected>
                                            {{ $category->name }}
                                        </option>
                                    @else
                                        <option class="event-category-list-item" value="{{ $category->id }}">
                                            {{ $category->name }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md 6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="AllDayEvent">All Day Event?</label>
                            <input type="checkbox" class="checkbox" id="AllDayEvent">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md 6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="EventStart">Start On</label>
                            @if(isset($event))
                                <input class="form-control required" id="EventStart" value="{{ explode(" ", $event->start_datetime)[0] }}">
                            @else
                                <input class="form-control required" id="EventStart"/>
                            @endif
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="col-no-padding col-lg-6 col-md-6 col-sm-12 col-xs-12" id="num-days-container" style="display:none">
                            <div class="form-group">
                                <label for="NumDays"># Days</label>
                                <input type="number" min="1" class="form-control validate-int" id="NumDays" value="1">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md 6 col-sm-12 col-xs-12 event-time-fields">
                        <div class="form-group">
                            <label for="EventFrom">Start Hour</label>
                            @if(isset($event))
                                <input class="time form-control required" id="EventFrom" autocomplete="off" value="{{ explode(" ", $event->start_datetime)[1] }}">
                            @else
                                <input class="time form-control required" id="EventFrom" autocomplete="off">
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 event-time-fields">
                        <div class="form-group">
                            <label>For</label>
                            @if(isset($event))
                                <input type="number" min='0.5' class="form-control required" id="EventTo" value="{{ (float) ($event->length_override / 60.0) }}">
                            @else
                                <input type="number" min='0.5' class="form-control required" id="EventTo" value="0.5">
                            @endif
                            <label for="EventTo">hours</label>
                        </div>
                    </div>
                    <div class="clearfix visible-lg"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" id="recurrence-select-parent" style="display:{{ isset($id) ? 'none' : 'inherit' }}">
                        <div class="form-group">
                            <label for="recurrence-type">Recurrence Type</label>
                            <select id="recurrence-type" class="form-control" name="recurrence-type" title="recurrence-type">
                                <option value="recur-none">One time event</option>
                                <option value="daily-recurrence">Daily</option>
                                <option value="only-weekdays">Every Weekday</option>
                                <option value="weekly-recurrence">Weekly</option>
                                <option value="monthly-recurrence">Monthly</option>
                                <option value="yearly-recurrence">Yearly</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 recurrence-options" id="daily-recurrence">
                        <div class="form-group">
                            <label for="daily-recurrence-value">Recur every</label>
                            <input class="form-control validate-int" type="number" min="1" value="1" id="daily-recurrence-value">days
                        </div>
                    </div>
                    <div class="col-xs-12 recurrence-options" id="weekly-recurrence">
                        <label>
                            <input type="checkbox" name="weekday" value="Sun"> Sun
                        </label>
                        <label>
                            <input type="checkbox" name="weekday" value="Mon"> Mon
                        </label>
                        <label>
                            <input type="checkbox" name="weekday" value="Tue"> Tue
                        </label>
                        <label>
                            <input type="checkbox" name="weekday" value="Wed"> Wed
                        </label>
                        <label>
                            <input type="checkbox" name="weekday" value="Thu"> Thu
                        </label>
                        <label>
                            <input type="checkbox" name="weekday" value="Fri"> Fri
                        </label>
                        <label>
                            <input type="checkbox" name="weekday" value="Sat"> Sat
                        </label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 recurrence-options" id="monthly-recurrence">
                        <div class="form-group">
                            <label for="monthly-recurrence-value">Recur every</label>
                            <input class="form-control validate-int" type="number" min="1" id="monthly-recurrence-value" value="1">months
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 recurrence-options" id="yearly-recurrence">
                        <div class="form-group">
                            <label for="yearly-recurrence-value">Recur every</label>
                            <input class="form-control validate-int" type="number" min="1" id="yearly-recurrence-value" value="1">years
                        </div>
                    </div>
                    <div class="col-xs-12 col-no-padding global-recurrence-options">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="recurrence-length-option">End Date</label>
                            <input type="radio" name="recurrence-length-option" title="recurrence-length-option" value="date" checked>
                            <div class="form-group">
                                <label for="recur-until-date"></label>
                                <input class="form-control length-option-input" type="text" name="recur-until-date" id="recur-until-date">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="recurrence-length-option">Fixed #</label>
                            <input type="radio" title="recurrence-length-option" name="recurrence-length-option" value="fixed">
                            <div class="form-group">
                                <label for="recur-n-times"></label>
                                <input class="form-control validate-int length-option-input" type="number" min="1" name="recur-n-times" id="recur-n-times" value="1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-sx-12">
                    <div class="col-xs-12" id="attendee-list">
                        <div class="row">
                            <p class="col-xs-12">Attendees</p>
                            <label class="col-xs-2">View: </label>
                            <div class="col-xs-3">
                                <a role="button" onclick="formConfig.showAllContacts()">All</a>
                            </div>
                            <div class="col-xs-4">
                                <a role="button" onclick="formConfig.viewAttending()">Attending</a>
                            </div>
                            <div class="col-xs-3">
                                <a href="/contact-form?returnurl=/event-form">Create</a>
                            </div>
                        </div>
                        <div id="contact-control">
                            <input type="text" id="contact-filter" class="form-control" placeholder="filter">
                            <ul id="contact-select">
                                @foreach($contacts as $contact)
                                    <li class="contact-list-item available-attendee" data-contact-id="{{ $contact->id }}" >
                                        <div class="contact-list-item-container">
                                            <span class="contact-list-item-name">
                                                <a role="button">{{ $contact->first_name." ".$contact->last_name }}</a>
                                            </span>
                                            <span>
                                                | <a role="button" class="contact-to-title-link" onclick="setTitle('{{ $contact->first_name.' '.$contact->last_name }}')">Set as title</a>
                                            </span>
                                            <span>
                                                | <a class="contact-profile-link" target="_blank" href="/contact-profile/{{ $contact->id }}">Profile</a>
                                            </span>
                                            <img class='expand-form-icon' src="/images/expand-icon.png" width="24px" height="24px"/>

                                        </div>
                                        <div class="row attendance-form">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <select title='attendance status select' class="form-control attendee-status">
                                                    <option value="RESERVED" selected>Scheduled</option>
                                                    <option value="ATTENDED">Attended</option>
                                                    <option value="ATTENDED_REFUNDED">Attended, Refunded</option>
                                                    <option value="ATTENDED_PARTIAL_REFUND">Attended, Partial refund</option>
                                                    <option value="CANCLED_REFUNDED">Canceled</option>
                                                    <option value="CANCLED_NOT_REFUNDED">Canceled, no refund</option>
                                                    <option value="CANCLED_PARTIAL_REFUND">Canceled, partial refund</option>
                                                    <option value="RESCHEDULED_REFUNDED">Rescheduled, refunded</option>
                                                    <option value="RESCHEDULED_NOT_REFUNDED">Reschedule, no refund</option>
                                                    <option value="RESCHEDULED_PARTIAL_REFUND">Rescheduled, partial refund</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <input class="form-control attendee-refund-percentage" type="number" min="0" placeholder="refund %" value="0.00">
                                            </div>
                                        </div>
                                        <div class="row attendance-form">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <textarea class="form-control attendance-form-notes" placeholder="notes">No notes yet.</textarea>
                                                <a role="button" class="delete-attendance-link">Delete</a>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="button-row">
                <div class="col-lg-2 col-xs-4">
                    <div class="form-group">
                        <input class="btn btn-primary" value="Save" type="submit" onclick="editOrCreate(event)">
                    </div>
                </div>
                <div class="col-lg-2 col-xs-4">
                    <div class="form-group">
                        <input class="btn btn-primary" value="Cancel" type="submit" onclick="window.open(getReturnUrl(), '_self')">
                    </div>
                </div>
                <div class="col-lg-2 col-xs-4" style="display: {{ isset($id) ? 'inherit' : 'none' }}">
                    <div class="form-group">
                        <input class="btn btn-primary" value="Delete" type="button"
                               onclick="deleteEvent({{ isset($id) ? $id : '' }}, {{ isset($event->event_group) ? $event->event_group : 'undefined' }})">
                    </div>
                </div>
            </div>
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Delete event or recurrence</h4>
                        </div>
                        <div class="modal-body">
                            <p>This event is part of a recurrence. Do you want to delete just this event or would you like
                                to delete the entire recurrence?
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" onclick="deleteRecurrence({{ isset($event->event_group) ? $event->event_group : 'undefined' }})">Entire Series</button>
                            <button type="button" class="btn btn-default" onclick="deleteEvent({{ isset($id) ? $id : '' }}, 'undefined')">Just this one</button>
                        </div>
                    </div>

                </div>
            </div>
            <div id="edit-recurrence-modal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Edit event or recurrence</h4>
                        </div>
                        <div class="modal-body">
                            <p>This event is part of a recurrence. Do you want to edit just this event or would you like
                                to edit the entire recurrence? You may choose to leave this event in the recurrence or
                                make it a stand alone event. Standalong events will not be updated when the recurrence is
                                updated.
                            </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-default" onclick="updateRecurrence({{ isset($event) ? $event->event_group : 'undefined' }})">Entire Series</button>
                            <button type="button" class="btn btn-default" onclick="$('#exception-buttons').show()">Just this event</button>
                        </div>
                        <div class="modal-footer" id="exception-buttons" style="display:none">
                            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="updateRecurringEvent({{ isset($id) ? $id : 'undefined' }}, false)">Keep in series</button>
                            <button type="button" class="btn btn-default" onclick="updateRecurringEvent({{ isset($id) ? $id : 'undefined' }}, true)">Remove from series</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
