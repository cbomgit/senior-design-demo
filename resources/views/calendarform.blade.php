@extends('layouts.single-col')

@section('stylesheets')
    <style type="text/css">

        .container-right {
            width: 100%;
        }

        #error-text {
            color:red;
        }

        #ajax-message {
            color: limegreen;
        }
    </style>

@endsection

@section('scripts')
    <script src="/js/formSaver.js"></script>
    <script>
        function saveForm() {

            var name = document.getElementById('name').value;

            var body = {
                "name": name,
            };

            formSaver.save(body);
        }

        $(document).ready(function() {
            formSaver.id = '{{ isset($id) ? '/'.$id : '' }}';
            formSaver.type = 'calendar';
            formSaver.method = '{{ isset($id) ? "PUT" : "POST" }}';
        });
    </script>
@endsection

@section('content')
<div class="panel panel-default">

    <div class="panel-heading">
        <span class="h4">{{  isset($calendar) ? 'Editing Calendar: ' . $calendar->name : 'Create a new calendar' }}</span>
        <span id="ajax-message" style="display: none;"></span>
        <span id="api_token" type="hidden" _token={{ $api_token }}></span>
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-8 col-xs-8">
                <p id="error-text"></p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">
                <div class="form-group">
                    <label for="name">Calendar Name</label>
                    <input class="form-control" name="name" id="name" type="text" value="{{ isset($calendar) ? $calendar->name : '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
                    <input class="btn btn-primary" value="Save" type="submit" onclick="saveForm()">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
                    <input class="btn btn-primary" value="Cancel" type="submit" onclick="formSaver.navToReturnUrl()">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4" style="display:{{ isset($id) ? 'inherit' : 'none' }}">
                <div class="form-group">
                    <input class="btn btn-primary" value="Delete" type="submit" onclick="formSaver.delete()">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
